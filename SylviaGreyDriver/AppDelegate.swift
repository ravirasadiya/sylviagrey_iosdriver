//
//  AppDelegate.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import Firebase
import Messages
import GoogleMaps

var dictverifyDriverDataModel = [String:Any]()
var driverData:[String:Any] = [:]
var obj:verifyDriverDataModel?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    /*----- 0-English, 1-Hindi 2-Gujarati -----*/
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var dataDict:[String: String]?
    var userCurrentLanguage = 0
    var navigation : UINavigationController?
   //Android key : AIzaSyAzoFYn6lvFHXFA3Z7ZVGsNPyCsilwrPZc
    //My Key : AIzaSyDqCKOFkanuy2uxe0ThywnILD8GJ4hYv4o
    let googleApiKey = "AIzaSyBsE4wyfrtrxtueWzJjDMu3lKynxI07o_E"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(googleApiKey)
        login()
        self.window?.makeKeyAndVisible()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        // Use Firebase library to configure APIs
        //        FirebaseApp.configure()
        let firebaseDatabaseReference = Database.database().reference()
        // Override point for customization after application launch.
        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //Messaging.messaging().apnsToken = deviceToken
        print(deviceToken)
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        })
    }
    func login (){
        let login =  UserDefaults.standard.bool(forKey: "LoginSuccess")
        if login == true {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let MainTabVC = mainStoryboard.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
            self.window?.rootViewController = MainTabVC
//            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let MainTabVC = mainStoryboard.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
//            self.window?.rootViewController = MainTabVC
        }else{
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigation =  UINavigationController.init(rootViewController: LoginVC)
            self.window?.rootViewController = navigation
        }
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
      //  self.login()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    override init() {
        super.init()
        FirebaseApp.configure()
        // not really needed unless you really need it
        Database.database().isPersistenceEnabled = true
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
        
    }
    //    // MARK: UISceneSession Lifecycle
    //
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        Messaging.messaging().appDidReceiveMessage(userInfo)
     
            if userInfo != nil {
                //                let id = userInfo["id"]
                //                let name = userInfo["name"]
                //                let address = userInfo["address"]
                //                let collection_date = userInfo["collection_date"]
                //                let collectiontime = userInfo["collection_time"]
                //                let delivery_date = userInfo["delivery_date"]
                //                let delivery_time = userInfo["delivery_time"]
                //                let total = userInfo["total"]
                //                let order_status_id = userInfo["order_status_id"]
                //                let arrUserInfo : Array<Any>! = ["\(id!)","\(name!)","\(address!)","\(collection_date!)","\(collectiontime!)","\(delivery_date!)","\(delivery_time!)","\(total!)","\(order_status_id!)"]
                //                var OrderDataDict = [[]]
                //                OrderDataDict.append(arrUserInfo)
                //   UserDefaults.standard.setValue(OrderDataDict, forKey: "OrderDataDict")
                //
                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                self.window?.rootViewController = DashBoardVC
                // Print full message.
                print("userInfo: \(userInfo)")
                // With swizzling disabled you must let Messaging know
                // Print full message.
                print(userInfo)
           
        }
//        else{
//            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigation =  UINavigationController.init(rootViewController: LoginVC)
//            self.window?.rootViewController = navigation
//        }
    }
}

extension AppDelegate:MessagingDelegate{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        print("Firebase registration token: \(String(describing: fcmToken))")
        UserDefaults.standard.setValue(fcmToken, forKey: "FCMToken")
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
}

