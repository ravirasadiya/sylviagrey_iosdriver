//
//  SocketIOManager.swift
//  SylviaGreyDriver
//
//  Created by Apple on 15/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation
import SocketIO
class SocketIOManager: NSObject {
  
    
    static let sharedInstance = SocketIOManager()
    
    //var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://192.168.1.XXX:3000")!)
    let manager = SocketManager(socketURL: URL(string: "http://13.235.73.21:9091/")!,config: [.log(true), .reconnectAttempts(-1), .reconnectWait(5), .reconnects(true), .version(.three)])
    var socket:SocketIOClient!
    override init() {
        super.init()
        
        socket = manager.defaultSocket
        
    }
    
    func addHandlers(){
        
        manager.defaultSocket.on(clientEvent: .connect) {data, ack in
            print("\nSocket connected\n")
        }
        
        manager.defaultSocket.on(clientEvent: .disconnect) {data, ack in
            print("\nSocket disconnected\n")
        }
        
        manager.defaultSocket.on(clientEvent: .error) {data, ack in
            print("\nSocket error\n")
        }
        
        manager.defaultSocket.on(clientEvent: .statusChange) {data, ack in
            print("\nStatus change: \(data)\n")
            self.socket.on(clientEvent:.websocketUpgrade) { (data, ack) in
                print("websocketUpgrade")
                print(data)
                print(ack)
            }
        }
        manager.defaultSocket.onAny { (event) in
            print("Event :\(event)")
        }
        
    }
    
    func establishConnection() {
//        socket = manager.defaultSocket;
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }

    func connectToServerWithUserName(param: [String:Any], completionHandler: @escaping (_ Dict: [[String: AnyObject]]?) -> Void) {
        
        self.socket.on(clientEvent: .connect) {data, ack in
            print("socket connected");
            let param = param
            print(Socket_URL)
//            self.socket.emit("driver_status", param)
            self.socket.emitWithAck("driver_status", param).timingOut(after: 0) { (data) in
                print(data)
            }
            print(param)
        };
        
    }
    
    func sendDriverRequest(){
        
    }

}
