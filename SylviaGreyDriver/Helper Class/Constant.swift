//
//  Constant.swift
//
//  Created by Trivedi Sagar on 10/18/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit


let appVersion = mainBundle.infoDictionary?[kBundleShortVersionKey] as! String
var osVersion = UIDevice.current.systemVersion

/*~~~~~ main bundle path ~~~~~*/
let mainBundle = Bundle.main

/*~~~~~ define all dictionary keys ~~~~~*/
let kBundleShortVersionKey = "CFBundleShortVersionString"

/*~~~~~ ApDelegate object ~~~~~*/
let kAppDelegate        = UIApplication.shared.delegate as! AppDelegate

/*~~~~~ API URL ~~~~~*/
let BASE_URL            = "http://shineinfosoft.in/api/"
let kUnAthorisedStatusCode = 111

/*~~~~~ API Key ~~~~~*/
let kPostcodeAthorisedkey = "kv0NXS-5pU6z2uywVF2rxw11778"

/*~~~~~ UserDefault define ~~~~~*/
let kLanguage           = "Language"


/*~~~~~ TextField define ~~~~~*/
let kTextFieldPading : CGFloat = 10

/*---------Socket URL*/
let Socket_URL = "http://13.235.73.21:9091/"

/*~~~~~ RGB Color ~~~~~*/
func RGB(red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat) -> UIColor {
    return UIColor(displayP3Red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
}

let kAppThemeborderColor = UIColor.black


let kAppThemePrimaryLightColor = RGB(red: 1, green: 161, blue: 159, alpha: 1)//R:1, G:161, B:159 //background color
let kAppThemePrimaryDarkColor = RGB(red: 1, green: 137, blue: 133, alpha: 1)//R:1, G:137, B:133 // statusbar
let kAppThemeAccentColor = RGB(red: 255, green: 255, blue: 255, alpha: 1)


let kAppButtonBGDeactivateColor = RGB(red: 151, green: 151, blue: 151, alpha: 1)//R:151, G:151, B:151
let kAppButtonTextDeactivateColor = RGB(red: 114, green: 114, blue: 114, alpha: 1)//R:114, G:114, B:114


let kAppButtonBGActiveColor = RGB(red: 255, green: 103, blue: 140, alpha: 1)//R:225, G:103, B:140
let kAppButtonTextActiveColor = RGB(red: 255, green: 255, blue: 255, alpha: 1)//R:225, G:103, B:140

let kAppTextSepColor = RGB(red: 151, green: 151, blue: 151, alpha: 1)//R:225, G:103, B:140

let kAppCartBGColor = RGB(red: 225, green: 103, blue: 140, alpha: 1)//R:225, G:103, B:140

/*~~~~~ Loading size ~~~~~*/
let kLoaderSize = CGSize(width: 50, height: 50)


/*~~~~~ segue identifiers ~~~~~*/


/*~~~~~ tableview cell identifier constant ~~~~~*/


/*~~~~~ collectionview cell identifier constant ~~~~~*/


/*~~~~~ current device size ~~~~~*/
let SystemSize = UIScreen.main.bounds.size

/*~~~~~ constant variable define ~~~~~*/
let PHONE_MAX_CHAR_SIZE = 10
let Maximum_Data_Display_Home = 6

/*~~~~~ custom font name ~~~~~*/
let kNunito_Bold = "Nunito-Bold"//Nunito-Bold
let kNunito_Regular = "Nunito-Regular"


/*~~~~~ custom font name ~~~~~*/
func kCustom_Font(fontName : String, fontSize : CGFloat) -> UIFont {
    return UIFont(name:fontName, size:fontSize)!
}

/*~~~~~ place holder image define ~~~~~*/
let kProductImagePlaceHolder = UIImage.init(named: "imgProduct")
let kCompanyImagePlaceHolder = UIImage.init(named: "imgCompany")
let kUserImagePlaceHolder = UIImage.init(named: "profile_img")

let kBusinessImagePlaceHolder = UIImage.init(named: "businessimage")

/*~~~~~ Notification center define ~~~~~*/

let kUpdateHomeBasketPrice = "UpdateHomeBasketPrice"
let kUpdateProductDetailViewBasketPrice = "UpdateProductDetailViewBasketPrice"
let kDismissLoaderView = "DismissLoaderView"
let kLogoutMoveLoginPage = "LogoutMoveLoginPage"

let kHomeViewUpdateNeed = "HomeViewUpdateNeed"

var kHelpPhoneNumber = "9898853228"
var kOrderPhoneNumber = "9898853228"

/*~~~~~ view controller identifiers ~~~~~*/

let kUpdateProductTops = "UpdateProductTops"
let kUpdateProductLaundry = "UpdateProductLaundry"
let kUpdateProductBedding = "UpdateProductBedding"
let kUpdateProductSuits = "UpdateProductSuits"
let kUpdateProductTrousers = "UpdateProductTrousers"
let kUpdateProductDresses = "UpdateProductDresses"
let kUpdateProductOutdoor = "UpdateProductOutdoor"
let kUpdateProductAccessories = "UpdateProductAccessories"
let kUpdateProductHome = "UpdateProductHome"
let kUpdateProductBusiness = "UpdateProductBusiness"


/*~~~~~ tableview cell identifier constant ~~~~~*/
let faqCell                  = "FaqCell"
let productItemCell          = "ProductItemCell"



/*~~~~~ collectionview cell identifier constant ~~~~~*/
let laundryItemCell          = "LaundryItemCell"


