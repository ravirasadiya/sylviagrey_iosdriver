//
//  PerformanceProgressBar.swift
//  SylviaGreyDriver
//
//  Created by Apple on 16/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation
import UIKit

class PerformanceProgressBar: UIView {
    
    let shapeLayer = CAShapeLayer()
    let trackLayer = CAShapeLayer()
    private var label = UILabel()//(frame: CGRect(x: 0, y: 1, width: 13.5, height: 20))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addLabel(lbltext: "55%")
        addProgressBar(radius: 5, progress: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      
        addLabel(lbltext: "55%")
        addProgressBar(radius: 5, progress: 0)
    }
    private func makeLabel(withText text: String) -> UILabel {
      
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
      //  label.frame =  CGRect(x: bounds.width / 2 ,y: bounds.height / 2,width: 5, height: 20)
       // label.text = text
        label.font = UIFont.systemFont(ofSize: labelSize)
        label.sizeToFit()
        label.center = pathCenter
        return label
    }
    public var labelSize: CGFloat = 20 {
        didSet {
            label.font = UIFont.systemFont(ofSize: labelSize)
            label.sizeToFit()
            configLabel()
        }
    }
    private func configLabel(){
        label.sizeToFit()
        label.center = pathCenter
    }
    private var pathCenter: CGPoint{
        get{ return self.convert(self.center, from:self.superview) }
    }
    func addLabel(lbltext:String)  {
       
       let label = makeLabel(withText: lbltext)
        label.sizeToFit()
   //     label.frame =  CGRect(x: 0 ,y:0,width: 13, height: 20)
        configLabel()
        self.addSubview(label)
      //  label.text = "55%"
        label.textColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
    }
    func addProgressBar(radius: CGFloat, progress: CGFloat) {
        
        let lineWidth = radius*0.2
        
        let circularPath = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: radius, startAngle: 0, endAngle: CGFloat.pi*2, clockwise: true)
        
        //TrackLayer
        trackLayer.path = circularPath.cgPath
        trackLayer.fillColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1) //UIColor.lightGray.cgColor
        trackLayer.strokeColor =  UIColor.clear.cgColor
        trackLayer.opacity = 0.5
        trackLayer.lineWidth = lineWidth
        trackLayer.lineCap = CAShapeLayerLineCap.square
        
        //BarLayer
        shapeLayer.path = circularPath.cgPath
        shapeLayer.fillColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1) //UIColor.clear.cgColor //#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        shapeLayer.strokeColor = kAppThemePrimaryLightColor.cgColor//UIColor.white.cgColor //UIColor.systemGreen.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.strokeEnd = 0
        shapeLayer.lineCap = CAShapeLayerLineCap.square
        
        
        
        //Rotate Shape Layer
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi/2, 0, 0, 1)
        
        //Shape Shadow
//        shapeLayer.shadowColor = UIColor.black.cgColor
//        shapeLayer.shadowOffset = CGSize(width: -7, height: 7)
//        shapeLayer.shadowRadius = 1
//        shapeLayer.shadowOpacity = 0.5
        
        //Animation
        loadProgress(percentage: progress)
        
        //LoadLayers
        layer.addSublayer(trackLayer)
        layer.addSublayer(shapeLayer)
    }
    func loadProgress(percentage: CGFloat) {
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.fromValue = 0
        basicAnimation.duration = 2
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.strokeEnd = percentage
        shapeLayer.add(basicAnimation, forKey: "basicStroke")
        
    }
}
