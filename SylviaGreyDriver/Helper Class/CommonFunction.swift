//
//  CommonFunction.swift
//  SylviaGreyDriver
//
//  Created by Apple on 12/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation
import UIKit

class CommonFunction  : NSObject {
    
    class func ShowAlert(alertTitle:String,alertMessage:String,okActionTitle:String) {
        let alertViewController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okActionTitle, style: .cancel) { _ in }
        alertViewController.addAction(okAction)
    }
    
        
    
}

