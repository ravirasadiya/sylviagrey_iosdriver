//
//  TomorrowViewModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 19/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class TomorrowViewModel: NSObject {
    var arrTomorrowOrderDataModel:[TomorrowModel] = [TomorrowModel]()
  
    var dictTomorrowOrderDataModel:[String:Any]?
    
    func TomorroworderRequest(driver_id:Int,completion: @escaping( _ ifResult: Bool,_ data:[String:Any],_ message:String) -> Void){
        
        WebServiceCall.callMethodWithURL(route: .tomorrowOrder(driver_id: driver_id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?,response:Any)   in
            print("responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((responseCode)!)")
            if responseCode != 200 {
                if let dictResult = responseValue{
                    if dictResult.isEmpty {
                        completion(false, responseValue ?? [:],responseValue?["message"] as? String ?? "")
                    }
                }
            }else{
                if let dictResult = responseValue{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                        for dict in arrResult{
//                            let obj = TomorrowModel
//                            self.arrTomorrowOrderDataModel.append(obj)
                        }
                    }
                    self.dictTomorrowOrderDataModel = (dictResult["data"] as? [String:Any])
                    completion(true,responseValue ?? [:],responseValue?["message"] as? String ?? "")
                }
            }
        }
    }
}
