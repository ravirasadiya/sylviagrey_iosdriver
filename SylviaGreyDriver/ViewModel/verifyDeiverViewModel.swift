//
//  verifyDeiverViewModel.swift
//  SylviaGreyDriver
//
//  Created by Vvk on 12/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class verifyDeiverViewModel: NSObject{
    
    var arrverifyDriverDataModel:[verifyDriverDataModel] = [verifyDriverDataModel]()
   
    
    
    func verifyDriver(contact:String,country_code:String,completion: @escaping( _ ifResult: Bool,_ data:[String:Any],_ status:Int,_ message:String) -> Void){
        
        WebServiceCall.callMethodWithURL(route: .verifyDriver(contact: contact, country_code: country_code)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?,response:Any)  in
            print("responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((responseCode))")
//            if responseCode != 200 {
//                if let dictResult = responseValue{
//                    if dictResult.isEmpty {
//                        completion(false, responseValue ?? [:],responseValue!["status"] as! Int,responseValue!["message"] as! String)
//                    }
//                }
//            }else{
                if let dictResult = responseValue{
                      if dictResult["message"] as? String == "Success" || dictResult["message"] as? String == "success" {
                    dictverifyDriverDataModel = (dictResult["data"] as! [String:Any])
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                        for dict in arrResult{
                             obj = verifyDriverDataModel(fromDictionary: dict)
                            self.arrverifyDriverDataModel.append(obj!)
                        }
                    }
                    completion(true,responseValue ?? [:],responseCode as! Int,responseValue!["message"] as! String)
                      }else{
                        completion(true,responseValue ?? [:],responseCode as! Int,responseValue!["message"] as! String)
                      }
                }
            }
//        }
    }
}
