//
//  declineorderRequestViewModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 18/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class declineorderRequestViewModel: NSObject{
    
    var arrDeclineorderDataModel:[DeclineOrderRequestModel] = [DeclineOrderRequestModel]()
    var dictDeclineorderOrderDataModel:[String:Any]?
    
    func declineorder(order_id:String,completion: @escaping( _ ifResult: Bool) -> Void){
        
        WebServiceCall.callMethodWithURL(route: .declineorderRequest(order_id: order_id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?,response:Any) in
            print("responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((responseCode)!)")
            if responseCode != 200 {
                if let dictResult = responseValue{
                    if dictResult.isEmpty {
                        completion(false)
                    }
                }
            }else{
                if let dictResult = responseValue{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                        for dict in arrResult{
//                            let obj = DeclineOrderModel(fromDictionary: dict)
//                            self.arrDeclineorderDataModel.append(obj)
                        }
                    }
             //       self.dictDeclineorderOrderDataModel = (dictResult["data"] as! [String:Any])
                    completion(true)
                }
            }
        }
    }
}
