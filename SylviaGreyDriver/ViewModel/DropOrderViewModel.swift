//
//  DropOrderViewModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 20/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class DropOrderViewModel: NSObject {
    func dropOrder(driver_id:Int,order_id:Int,completion: @escaping( _ ifResult: Bool,_ statusCode:Int,_ Data:[[String:Any]],_ message:String,_ response:[String:Any]) -> Void){
        WebServiceCall.callMethodWithURL(route: .pickupOrder(driver_id: driver_id, order_id: order_id)) { (statusCode, responseValue, error, responseData) in
            print("Pending ApI responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((statusCode)!)")
            if statusCode != 200 {
                if let dictResult = responseValue{
                    if dictResult.isEmpty {
                        completion(false,statusCode ?? 0,responseValue?["data"] as? [[String : Any]] ?? [["":""]],responseValue?["message"] as? String ?? "",responseValue ?? [:])
                    }
                }
            }else{
                if let dictResult = responseValue{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                        for dict in arrResult{
                            //                            let obj = pendingRequestModel(from: arrResult as! Decoder)
                            //                            self.arrPendingRequestDataModel.append(obj)
                        }
                    }
                    //       self.dictverifyDriverDataModel = (dictResult["data"] as! [String:Any])
                    completion(true,statusCode ?? 0,responseValue?["data"] as? [[String : Any]] ?? [["":""]],responseValue?["message"] as? String ?? "",responseValue ?? [:])
                }
            }
            //            responsedata = [["responseCode":responseCode],["ResponseValue":responseValue]]
            print(response)
        }
        // return responsedata
    }
}

