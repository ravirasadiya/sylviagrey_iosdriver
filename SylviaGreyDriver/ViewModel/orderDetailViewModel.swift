//
//  orderDetailViewModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 25/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation


class orderDetailViewModel: NSObject {
    var arrOrderDetailModel:[orderDetailModel] = [orderDetailModel]()
    
    //let dictDataInvoiceModel:[String:Any] = [:]
    
    func orderDetail(order_id:Int,completion: @escaping( _ ifResult: Bool,_ data:[String:Any],_ statusCode:Int) -> Void){
        
        WebServiceCall.callMethodWithURL(route: .orderDetail(order_id: order_id)) { [self] (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?,response:Any)   in
            print("responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((responseCode)!)")
            if responseCode != 200 {
                if let dictResult = responseValue{
                    if dictResult.isEmpty {
                        completion(false, responseValue ?? [:],responseCode ?? 0)
                    }
                }
            }else{
                if let dictResult = responseValue{
                    if dictResult["status"] as? String == "Success"{
                        if  let arrResult = dictResult["data"] as? [[String:Any]] {
                            for dict in arrResult{
                                let obj = orderDetailModel(dictionary: dict as NSDictionary)
                                self.arrOrderDetailModel.append(obj!)
                              
                            }
                        }
                    }
                    completion(true,responseValue ?? [:],responseCode ?? 0)
                }
            }
            
        }
    }
}
