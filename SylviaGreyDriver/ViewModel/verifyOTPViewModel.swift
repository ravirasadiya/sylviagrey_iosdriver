//
//  verifyOTPViewModel.swift
//  SylviaGreyDriver
//
//  Created by Vvk on 12/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class verifyOTPViewModel: NSObject{
    var arrotpVerifyDataModel:[otpVerifyDataModel] = [otpVerifyDataModel]()
    var dicotpVerifyDataModel:[String:Any]?
    func verifyOTP(contact:String,otp:String,token:String,country_code:String,completion: @escaping( _ ifResult: Bool,_ data:[String:Any],_ message:String,_ statusCode:Int) -> Void){
        
        let params = ["contact":contact,"otp":otp,"token":token,"country_code":country_code]
        
        WebServiceCall.callMethodWithURL(route: .OtpVerify(params)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?,response:Any) in
            print("responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((responseCode)!)")
            if responseCode == 200 {
                if let dictResult = responseValue{
                    if dictResult.isEmpty != true {
//                        let obj = otpVerifyDataModel(fromDictionary: (dictResult["data"] as? [String : Any])!)
//                        self.arrotpVerifyDataModel.append(obj)
                     //   self.dicotpVerifyDataModel = (dictResult["data"] as? [String:Any])!
                        completion(true,responseValue!,(responseValue!["message"] as? String)!,responseValue!["status"]  as! Int)
                    }
                }
            }else{
                completion(false, responseValue!,(responseValue!["message"] as? String)!,responseValue!["status"]  as! Int)
            }
        }
    }
}
