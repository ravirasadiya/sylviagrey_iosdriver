//
//  CompleteOrderListForDriverViewModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 19/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class CompleteOrderListForDriverViewModel: NSObject {
    var arrInvoiceModel:[InvoiceModel] = [InvoiceModel]()
    
    //let dictDataInvoiceModel:[String:Any] = [:]
    
    func invoiceData(driver_id:Int,completion: @escaping( _ ifResult: Bool,_ data:[String:Any]) -> Void){
        WebServiceCall.callMethodWithURL(route: .invoiceData(driver_id: 5)) { [self] (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?,response:Any)   in
            print("responseValue=\(responseValue as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((responseCode)!)")
            if responseCode != 200 {
                if let dictResult = responseValue{
                    if dictResult.isEmpty {
                        completion(false, responseValue!)
                    }
                }
            }else{
                if let dictResult = responseValue{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = responseValue as? [[String:Any]] { //dictResult["data"] as? [[String:Any]]{
                        for dict in arrResult{
                            let obj = InvoiceModel(dictionary: dict as? NSDictionary ?? [:])
                            self.arrInvoiceModel.append(obj!)
                           // self.arrTomorrowOrderDataModel.append(obj)
                        }
                    }
                    print(arrInvoiceModel)
                    completion(true,responseValue!)
                }
            }
        }
          
    }
}
