//
//  orderDetailDataModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 25/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

public class orderDetailDataModel {
    public var customer_name : String?
    public var id : Int?
    public var address : String?
    public var collection_date : String?
    public var collection_time : String?
    public var delivery_date : String?
    public var delivery_time : String?
    public var total : String?
    public var latitude : String?
    public var longitude : String?
    public var item : Array<orderDetailItemModel>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [orderDetailDataModel]
    {
        var models:[orderDetailDataModel] = []
        for item in array
        {
            models.append(orderDetailDataModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
    required public init?(dictionary: NSDictionary) {

        customer_name = dictionary["customer_name"] as? String
        id = dictionary["id"] as? Int
        address = dictionary["address"] as? String
        collection_date = dictionary["collection_date"] as? String
        collection_time = dictionary["collection_time"] as? String
        delivery_date = dictionary["delivery_date"] as? String
        delivery_time = dictionary["delivery_time"] as? String
        total = dictionary["total"] as? String
        latitude = dictionary["latitude"] as? String
        longitude = dictionary["longitude"] as? String
        if (dictionary["item"] != nil) { item = orderDetailItemModel.modelsFromDictionaryArray(array: dictionary["item"] as! NSArray) }
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.customer_name, forKey: "customer_name")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.address, forKey: "address")
        dictionary.setValue(self.collection_date, forKey: "collection_date")
        dictionary.setValue(self.collection_time, forKey: "collection_time")
        dictionary.setValue(self.delivery_date, forKey: "delivery_date")
        dictionary.setValue(self.delivery_time, forKey: "delivery_time")
        dictionary.setValue(self.total, forKey: "total")
        dictionary.setValue(self.latitude, forKey: "latitude")
        dictionary.setValue(self.longitude, forKey: "longitude")

        return dictionary
    }

}
