//
//  orderDetailModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 25/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

public class orderDetailModel {
    public var status : Int?
    public var message : String?
    public var data : orderDetailDataModel?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Json4Swift_Base Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [orderDetailModel]
    {
        var models:[orderDetailModel] = []
        for item in array
        {
            models.append(orderDetailModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Json4Swift_Base Instance.
*/
    required public init?(dictionary: NSDictionary) {

        status = dictionary["status"] as? Int
        message = dictionary["message"] as? String
        if (dictionary["data"] != nil) { data = orderDetailDataModel(dictionary: dictionary["data"] as! NSDictionary) }
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.data?.dictionaryRepresentation(), forKey: "data")

        return dictionary
    }

}
