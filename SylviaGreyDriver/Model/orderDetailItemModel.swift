//
//  orderDetailItemModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 25/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

public class orderDetailItemModel {
    public var id : Int?
    public var order_id : Int?
    public var customer_id : Int?
    public var product_id : Int?
    public var service_id : Int?
    public var category_id : Int?
    public var subcategory_id : Int?
    public var quantity : String?
    public var product_name : String?
    public var product_price : String?
    public var offerprice : String?
    public var count : Int?
    public var shipping_price : String?
    public var created_at : String?
    public var updated_at : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let item_list = Item.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Item Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [orderDetailItemModel]
    {
        var models:[orderDetailItemModel] = []
        for item in array
        {
            models.append(orderDetailItemModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let item = Item(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Item Instance.
*/
    required public init?(dictionary: NSDictionary) {

        id = dictionary["id"] as? Int
        order_id = dictionary["order_id"] as? Int
        customer_id = dictionary["customer_id"] as? Int
        product_id = dictionary["product_id"] as? Int
        service_id = dictionary["service_id"] as? Int
        category_id = dictionary["category_id"] as? Int
        subcategory_id = dictionary["subcategory_id"] as? Int
        quantity = dictionary["quantity"] as? String
        product_name = dictionary["product_name"] as? String
        product_price = dictionary["product_price"] as? String
        offerprice = dictionary["offerprice"] as? String
        count = dictionary["count"] as? Int
        shipping_price = dictionary["shipping_price"] as? String
        created_at = dictionary["created_at"] as? String
        updated_at = dictionary["updated_at"] as? String
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.order_id, forKey: "order_id")
        dictionary.setValue(self.customer_id, forKey: "customer_id")
        dictionary.setValue(self.product_id, forKey: "product_id")
        dictionary.setValue(self.service_id, forKey: "service_id")
        dictionary.setValue(self.category_id, forKey: "category_id")
        dictionary.setValue(self.subcategory_id, forKey: "subcategory_id")
        dictionary.setValue(self.quantity, forKey: "quantity")
        dictionary.setValue(self.product_name, forKey: "product_name")
        dictionary.setValue(self.product_price, forKey: "product_price")
        dictionary.setValue(self.offerprice, forKey: "offerprice")
        dictionary.setValue(self.count, forKey: "count")
        dictionary.setValue(self.shipping_price, forKey: "shipping_price")
        dictionary.setValue(self.created_at, forKey: "created_at")
        dictionary.setValue(self.updated_at, forKey: "updated_at")

        return dictionary
    }

}
