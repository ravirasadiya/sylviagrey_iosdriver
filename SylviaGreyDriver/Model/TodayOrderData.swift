/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
class TodayOrderData : NSObject,NSCoding {
    
    
    var collection : [TodayOrderCollection]!
    let delivery : [String]?
    let all : [TodayOrderAll]!
    
    
    init(fromDictionary dictionary: [String:Any] ){
        collection = dictionary["collection"] as? [TodayOrderCollection]
        delivery = dictionary["delivery"] as? [String]
        all = dictionary["data"] as? [TodayOrderAll]
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if  collection != nil{
            dictionary["collection"] = collection
        }
        if delivery != nil{
            dictionary["delivery"] = delivery
        }
        if all != nil{
            dictionary["all"] = all
        }
        return dictionary
    }
    
    func encode(with coder: NSCoder) {
        if collection != nil{
            coder.encode(collection, forKey: "collection")
        }
        if delivery != nil{
            coder.encode(delivery, forKey: "delivery")
        }
        if all != nil{
            coder.encode(all, forKey: "all")
        }
    }
    
    required init?(coder: NSCoder) {
        collection = coder.decodeObject(forKey: "collection") as? [TodayOrderCollection]
        delivery = coder.decodeObject(forKey: "delivery") as? [String]
        all = coder.decodeObject(forKey: "all") as? [TodayOrderAll]
    }
}
