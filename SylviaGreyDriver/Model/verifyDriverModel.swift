//
//  verifyDriverModel.swift
//  SylviaGreyDriver
//
//  Created by Vvk on 11/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation

class verifyDriverModel : NSObject , Codable{
    
    
    let status : Int?
    let message : String?
    let data : Data?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case data = "data"
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
    }
    
}
