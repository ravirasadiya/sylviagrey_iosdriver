/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class  InvoiceDataModel {
	public var order_id : Int?
	public var details : String?
	public var order_status_id : Int?
	public var invoice_path : String?
	public var collection_time : String?
	public var collection_date : String?
	public var delivery_time : String?
	public var delivery_date : String?
	public var address : String?
	public var name : String?
	public var item : Array<InvoiceItemsModel>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [InvoiceDataModel]
    {
        var models:[InvoiceDataModel] = []
        for item in array
        {
            models.append(InvoiceDataModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		order_id = dictionary["order_id"] as? Int
		details = dictionary["details"] as? String
		order_status_id = dictionary["order_status_id"] as? Int
		invoice_path = dictionary["invoice_path"] as? String
		collection_time = dictionary["collection_time"] as? String
		collection_date = dictionary["collection_date"] as? String
		delivery_time = dictionary["delivery_time"] as? String
		delivery_date = dictionary["delivery_date"] as? String
		address = dictionary["address"] as? String
		name = dictionary["name"] as? String
        if (dictionary["item"] != nil) { item = InvoiceItemsModel.modelsFromDictionaryArray(array: dictionary["item"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.order_id, forKey: "order_id")
		dictionary.setValue(self.details, forKey: "details")
		dictionary.setValue(self.order_status_id, forKey: "order_status_id")
		dictionary.setValue(self.invoice_path, forKey: "invoice_path")
		dictionary.setValue(self.collection_time, forKey: "collection_time")
		dictionary.setValue(self.collection_date, forKey: "collection_date")
		dictionary.setValue(self.delivery_time, forKey: "delivery_time")
		dictionary.setValue(self.delivery_date, forKey: "delivery_date")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.name, forKey: "name")

		return dictionary
	}

}
