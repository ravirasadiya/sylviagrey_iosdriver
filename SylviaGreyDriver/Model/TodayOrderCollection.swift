/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
class  TodayOrderCollection : NSObject,NSCoding {
	let id : Int?
	let reference : String?
	let courier_id : String?
	let courier : String?
	let customer_id : Int?
	let email : String?
	let mail_status : Int?
    var address : String?
	let order_status_id : Int?
	let payment : String?
	let discounts : String?
	let total_products : String?
	let total_shipping : String?
	let tax : String?
	let total : String?
	let total_paid : String?
	let invoice : String?
	let invoice_path : String?
	let label_url : String?
	let tracking_number : String?
	let created_at : String?
	let updated_at : String?
	let collection_time : String?
	let delivery_time : String?
	let collection_date : String?
	let delivery_date : String?
	let driver_id : Int?
	let postcode : String?
	let delivery_note : String?
	let check_accept : Int?
	let reason_cancel : String?
	let latitude : String?
    var longitude : String?


    init(fromDictionary dictionary: [String:Any] ){
        id = dictionary["id"] as? Int
        reference = dictionary["reference"] as? String
        courier_id = dictionary["courier_id"] as? String
        courier = dictionary["courier"] as? String
        customer_id = dictionary["customer_id"] as? Int
        email = dictionary["email"] as? String
        mail_status = dictionary["mail_status"] as? Int
        address = dictionary["address"] as? String
        order_status_id = dictionary["order_status_id"] as? Int
        payment = dictionary["payment"] as? String
        discounts = dictionary["discounts"] as? String
        total_products = dictionary["total_products"] as? String
        total_shipping = dictionary["total_shipping"] as? String
        tax = dictionary["tax"] as? String
        total = dictionary["total"] as? String
        created_at = dictionary["created_at"] as? String
        updated_at = dictionary["updated_at"] as? String
        total_paid = dictionary["total_paid"] as? String
        address = dictionary["address"] as? String
        invoice = dictionary["invoice"] as? String
        invoice_path = dictionary["invoice_path"] as? String
        label_url = dictionary["label_url"] as? String
        tracking_number = dictionary["tracking_number"] as? String
        collection_time = dictionary["collection_time"] as? String
        delivery_date = dictionary["delivery_date"] as? String
        driver_id = dictionary["driver_id"] as? Int
        postcode = dictionary["postcode"] as? String
        delivery_note = dictionary["delivery_note"] as? String
        check_accept = dictionary["check_accept"] as? Int
        reason_cancel = dictionary["reason_cancel"] as? String
        latitude = dictionary["latitude"] as? String
        longitude = dictionary["longitude"] as? String
        delivery_time = dictionary["delivery_time"] as? String
        collection_date = dictionary["collection_date"] as? String
    }
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if reference != nil{
            dictionary["reference"] = reference
        }
        if courier_id != nil{
            dictionary["courier_id"] = courier_id
        }
        if courier != nil{
            dictionary["courier"] = courier
        }
        if customer_id != nil{
            dictionary["customer_id"] = customer_id
        }
        if email != nil{
            dictionary["email"] = email
        }
        if mail_status != nil{
            dictionary["mail_status"] = mail_status
        }
        if address != nil{
            dictionary["address"] = address
        }
        if order_status_id != nil{
            dictionary["order_status_id"] = order_status_id
        }
        if payment != nil{
            dictionary["payment"] = payment
        }
        if discounts != nil{
            dictionary["discounts"] = discounts
        }
        if total_products != nil{
            dictionary["total_products"] = total_products
        }
        if total_shipping != nil{
            dictionary["total_shipping"] = total_shipping
        }
        if tax != nil{
            dictionary["tax"] = tax
        }
        if total != nil{
            dictionary["total"] = total
        }
        if created_at != nil{
            dictionary["created_at"] = created_at
        }
        if updated_at != nil{
            dictionary["updated_at"] = updated_at
        }
        if total_paid != nil{
            dictionary["total_paid"] = total_paid
        }
        if address != nil{
            dictionary["address"] = address
        }
        if invoice != nil{
            dictionary["invoice"] = invoice
        }
        if invoice_path != nil{
            dictionary["invoice_path"] = invoice_path
        }
        if label_url != nil{
            dictionary["label_url"] = label_url
        }
        if tracking_number != nil{
            dictionary["tracking_number"] = tracking_number
        }
        if collection_time != nil{
            dictionary["collection_time"] = collection_time
        }
        if delivery_date != nil{
            dictionary["delivery_date"] = delivery_date
        }
        if driver_id != nil{
            dictionary["driver_id"] = driver_id
        }
        if postcode != nil{
            dictionary["postcode"] = postcode
        }
        if delivery_note != nil{
            dictionary["delivery_note"] = delivery_note
        }
        if check_accept != nil{
            dictionary["check_accept"] = check_accept
        }
        if reason_cancel != nil{
            dictionary["reason_cancel"] = reason_cancel
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        } //delivery_time
        if delivery_time != nil{
            dictionary["delivery_time"] = delivery_time
        }
        if collection_date != nil{
            dictionary["collection_date"] = collection_date
        }
        return dictionary
    }
    func encode(with coder: NSCoder) {
        if id != nil{
            coder.encode(id, forKey: "id")
        }
        if reference != nil{
            coder.encode(reference, forKey: "reference")
        }
        if courier_id != nil{
            coder.encode(courier_id, forKey: "courier_id")
        }
        if courier != nil{
            coder.encode(courier, forKey: "courier")
        }
        if customer_id != nil{
            coder.encode(customer_id, forKey: "customer_id")
        }
        if email != nil{
            coder.encode(email, forKey: "email")
        }
        if mail_status != nil{
            coder.encode(mail_status, forKey: "mail_status")
        }
        if address != nil{
            coder.encode(address, forKey: "address")
        }
        if order_status_id != nil{
            coder.encode(order_status_id, forKey: "order_status_id")
        }
        if payment != nil{
            coder.encode(payment, forKey: "payment")
        }
        if discounts != nil{
            coder.encode(discounts, forKey: "discounts")
        }
        if total_products != nil{
            coder.encode(total_products, forKey: "total_products")
        }
        if total_shipping != nil{
            coder.encode(total_shipping, forKey: "total_shipping")
        }
        if tax != nil{
            coder.encode(tax, forKey: "tax")
        }
        if total != nil{
            coder.encode(total, forKey: "total")
        }
        if created_at != nil{
            coder.encode(created_at, forKey: "created_at")
        }
        if updated_at != nil{
            coder.encode(updated_at, forKey: "updated_at")
        }
        if total_paid != nil{
            coder.encode(total_paid, forKey: "total_paid")
        }
        if address != nil{
            coder.encode(address, forKey: "address")
        }
        if invoice != nil{
            coder.encode(invoice, forKey: "invoice")
        }
        if invoice_path != nil{
            coder.encode(invoice_path, forKey: "invoice_path")
        }
        if label_url != nil{
            coder.encode(label_url, forKey: "label_url")
        }
        if tracking_number != nil{
            coder.encode(tracking_number, forKey: "tracking_number")
        }
        if collection_time != nil{
            coder.encode(collection_time, forKey: "collection_time")
        }
        if delivery_date != nil{
            coder.encode(delivery_date, forKey: "delivery_date")
        }
        if driver_id != nil{
            coder.encode(driver_id, forKey: "driver_id")
        }
        
        if postcode != nil{
            coder.encode(postcode, forKey: "postcode")
        }
        if delivery_note != nil{
            coder.encode(delivery_note, forKey: "delivery_note")
        }
        if check_accept != nil{
            coder.encode(check_accept, forKey: "check_accept")
        }
        if reason_cancel != nil{
            coder.encode(reason_cancel, forKey: "reason_cancel")
        }
        if latitude != nil{
            coder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            coder.encode(longitude, forKey: "longitude")
        }//delivery_time
        if delivery_time != nil{
            coder.encode(delivery_time, forKey: "delivery_time")
        }
        if collection_date != nil{
            coder.encode(collection_date, forKey: "collection_date")
        }
    }
    
    required init(coder:NSCoder) {
        id = coder.decodeObject(forKey: "id") as? Int
        reference = coder.decodeObject(forKey: "reference") as? String
        courier_id = coder.decodeObject(forKey: "courier_id") as? String
        courier = coder.decodeObject(forKey: "courier") as? String
        customer_id = coder.decodeObject(forKey: "customer_id") as? Int
        email = coder.decodeObject(forKey: "email") as? String
        mail_status = coder.decodeObject(forKey: "mail_status") as? Int
        address = coder.decodeObject(forKey: "status") as? String
        order_status_id = coder.decodeObject(forKey: "order_status_id") as? Int
        payment = coder.decodeObject(forKey: "payment") as? String
        discounts = coder.decodeObject(forKey: "discounts") as? String
        total_products = coder.decodeObject(forKey: "total_products") as? String
        total_shipping = coder.decodeObject(forKey: "total_shipping") as? String
        tax = coder.decodeObject(forKey: "tax") as? String
        total = coder.decodeObject(forKey: "total") as? String
        created_at = coder.decodeObject(forKey: "created_at") as? String
        updated_at = coder.decodeObject(forKey: "updated_at") as? String
        total_paid = coder.decodeObject(forKey: "total_paid") as? String
        address = coder.decodeObject(forKey: "address") as? String
        invoice = coder.decodeObject(forKey: "invoice") as? String
        invoice_path = coder.decodeObject(forKey: "invoice_path") as? String
        label_url = coder.decodeObject(forKey: "label_url") as? String
        tracking_number = coder.decodeObject(forKey: "tracking_number") as? String
        collection_time = coder.decodeObject(forKey: "collection_time") as? String
        delivery_date = coder.decodeObject(forKey: "delivery_date") as? String
        driver_id = coder.decodeObject(forKey: "driver_id") as? Int
        postcode = coder.decodeObject(forKey: "postcode") as? String
        delivery_note = coder.decodeObject(forKey: "delivery_note") as? String
        check_accept = coder.decodeObject(forKey: "check_accept") as? Int
        reason_cancel = coder.decodeObject(forKey: "reason_cancel") as? String
        latitude = coder.decodeObject(forKey: "latitude") as? String
        longitude = coder.decodeObject(forKey: "longitude") as? String
        longitude = coder.decodeObject(forKey: "longitude") as? String //delivery_time
        delivery_time = coder.decodeObject(forKey: "delivery_time") as? String
        collection_date = coder.decodeObject(forKey: "collection_date") as? String
    }

}
