//
//  driverStatusModel.swift
//  SylviaGreyDriver
//
//  Created by Apple on 12/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class driverStatusModel: NSObject,NSCoding {
    var latitude: CLLocationCoordinate2D?
    var longitude: CLLocationCoordinate2D?
    var driver_id:Int?
    var status:String?
    var time:String?
    
    init(fromDictionary dictionary: [String:Any] ){
        latitude = dictionary["latitude"] as? CLLocationCoordinate2D
        longitude = dictionary["longitude"] as? CLLocationCoordinate2D
        driver_id = dictionary["driver_id"] as? Int
        status = dictionary["status"] as? String
        time = dictionary["time"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if  latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if driver_id != nil{
            dictionary["driver_id"] = driver_id
        }
        if status != nil{
            dictionary["status"] = status
        }
        if time != nil{
            dictionary["time"] = time
        }
        return dictionary
    }
    
    func encode(with coder: NSCoder) {
        if latitude != nil{
            coder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            coder.encode(longitude, forKey: "longitude")
        }
        if driver_id != nil{
            coder.encode(driver_id, forKey: "driver_id")
        }
        if status != nil{
            coder.encode(status, forKey: "status")
        }
        if time != nil{
            coder.encode(time, forKey: "time")
        }
    }
    
    required init?(coder: NSCoder) {
        longitude = coder.decodeObject(forKey: "longitude") as? CLLocationCoordinate2D
        latitude = coder.decodeObject(forKey: "latitude") as? CLLocationCoordinate2D
        driver_id = coder.decodeObject(forKey: "driver_id") as? Int
        status = coder.decodeObject(forKey: "status") as? String
        time = coder.decodeObject(forKey: "time") as? String
    }
}
