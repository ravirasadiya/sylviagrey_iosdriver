/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
class TodayOrderModel: NSObject,NSCoding {
  
	let status : Int?
	let message : String?
	var data : TodayOrderData?

    init(fromDictionary dictionary: [String:Any] ){
        status = dictionary["status"] as? Int
        message = dictionary["message"] as? String
        data = dictionary["data"] as? TodayOrderData
        
    }
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if status != nil{
            dictionary["status"] = status
        }
        if message != nil{
            dictionary["message"] = message
        }
        if data != nil{
            dictionary["data"] = data
        }
        return dictionary
    }
    
    func encode(with coder: NSCoder) {
        if status != nil{
            coder.encode(status, forKey: "status")
        }
        if message != nil{
            coder.encode(message, forKey: "message")
        }
        if data != nil{
            coder.encode(data, forKey: "data")
        }
    }

    required init?(coder: NSCoder) {
        status = coder.decodeObject(forKey: "status") as? Int
        message = coder.decodeObject(forKey: "message") as? String
        data = coder.decodeObject(forKey: "data") as? TodayOrderData
    }
    
}
