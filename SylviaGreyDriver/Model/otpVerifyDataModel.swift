//
//  otpVerifyDataModel.swift
//  SylviaGreyDriver
//
//  Created by Vvk on 12/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit

import Foundation
class otpVerifyDataModel :  NSObject, NSCoding {
    var id : Int?
    var vehicle_type : String?
    var name : String?
    var email : String?
    var contact : Int?
    var country_code : String?
    var profile : String?
    var status : Int?
    var password : String?
    var otp : Int?
    var vehicle_reg_no : String?
    var licence_no : String?
    var bank_acc_no : String?
    var bank_name : String?
    var acc_holder_name : String?
    var created_at : String?
    var updated_at : String?
    var postalcode : String?
    var address : String?
    var police_certificate : String?
    var token : String?
    var email_verified_at : String?
    var vehicle_name : String?
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["id"] as? Int
        vehicle_type = dictionary["vehicle_type"] as? String
        name = dictionary["name"] as? String
        email = dictionary["email"] as? String
        contact = dictionary["contact"] as? Int
        country_code = dictionary["country_code"] as? String
        profile = dictionary["profile"] as? String
        status = dictionary["status"] as? Int
        password = dictionary["password"] as? String
        otp = dictionary["otp"] as? Int
        vehicle_reg_no = dictionary["vehicle_reg_no"] as? String
        licence_no = dictionary["licence_no"] as? String
        bank_acc_no = dictionary["bank_acc_no"] as? String
        bank_name = dictionary["bank_name"] as? String
        acc_holder_name = dictionary["acc_holder_name"] as? String
        created_at = dictionary["created_at"] as? String
        updated_at = dictionary["updated_at"] as? String
        postalcode = dictionary["postalcode"] as? String
        address = dictionary["address"] as? String
        police_certificate = dictionary["police_certificate"] as? String
        token = dictionary["token"] as? String
        email_verified_at = dictionary["email_verified_at"] as? String
        vehicle_name = dictionary["vehicle_name"] as? String
       
    }
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if vehicle_type != nil{
            dictionary["vehicle_type"] = vehicle_type
        }
        if name != nil{
            dictionary["name"] = name
        }
        if email != nil{
            dictionary["email"] = email
        }
        if contact != nil{
            dictionary["contact"] = contact
        }
        if country_code != nil{
            dictionary["country_code"] = country_code
        }
        if profile != nil{
            dictionary["profile"] = profile
        }
        if status != nil{
            dictionary["status"] = status
        }
        if password != nil{
            dictionary["password"] = password
        }
        if otp != nil{
            dictionary["otp"] = otp
        }
        if vehicle_reg_no != nil{
            dictionary["vehicle_reg_no"] = vehicle_reg_no
        }
        if licence_no != nil{
            dictionary["licence_no"] = licence_no
        }
        if bank_acc_no != nil{
            dictionary["bank_acc_no"] = bank_acc_no
        }
        if bank_name != nil{
            dictionary["bank_name"] = bank_name
        }
        if acc_holder_name != nil{
            dictionary["acc_holder_name"] = acc_holder_name
        }
        if created_at != nil{
            dictionary["created_at"] = created_at
        }
        if updated_at != nil{
            dictionary["updated_at"] = updated_at
        }
        if postalcode != nil{
            dictionary["postalcode"] = postalcode
        }
        if address != nil{
            dictionary["address"] = address
        }
        if police_certificate != nil{
            dictionary["police_certificate"] = police_certificate
        }
        if token != nil{
            dictionary["token"] = token
        }
        if email_verified_at != nil{
            dictionary["email_verified_at"] = email_verified_at
        }
        if vehicle_name != nil{
            dictionary["vehicle_name"] = vehicle_name
        }
        
        
        return dictionary
    }
    func encode(with coder: NSCoder) {
        if id != nil{
            coder.encode(id, forKey: "id")
        }
        if vehicle_type != nil{
            coder.encode(vehicle_type, forKey: "vehicle_type")
        }
        if name != nil{
            coder.encode(name, forKey: "name")
        }
        if email != nil{
            coder.encode(email, forKey: "email")
        }
        if contact != nil{
            coder.encode(contact, forKey: "contact")
        }
        if country_code != nil{
            coder.encode(country_code, forKey: "country_code")
        }
        if profile != nil{
            coder.encode(profile, forKey: "profile")
        }
        if status != nil{
            coder.encode(status, forKey: "status")
        }
        if password != nil{
            coder.encode(password, forKey: "password")
        }
        if otp != nil{
            coder.encode(otp, forKey: "otp")
        }
        if vehicle_reg_no != nil{
            coder.encode(vehicle_reg_no, forKey: "vehicle_reg_no")
        }
        if licence_no != nil{
            coder.encode(licence_no, forKey: "licence_no")
        }
        if bank_acc_no != nil{
            coder.encode(bank_acc_no, forKey: "bank_acc_no")
        }
        if bank_name != nil{
            coder.encode(bank_name, forKey: "bank_name")
        }
        if acc_holder_name != nil{
            coder.encode(acc_holder_name, forKey: "acc_holder_name")
        }
        if created_at != nil{
            coder.encode(created_at, forKey: "created_at")
        }
        if updated_at != nil{
            coder.encode(updated_at, forKey: "updated_at")
        }
        if postalcode != nil{
            coder.encode(postalcode, forKey: "postalcode")
        }
        if address != nil{
            coder.encode(address, forKey: "address")
        }
        if police_certificate != nil{
            coder.encode(police_certificate, forKey: "police_certificate")
        }
        if token != nil{
            coder.encode(token, forKey: "token")
        }
        if email_verified_at != nil{
            coder.encode(email_verified_at, forKey: "email_verified_at")
        }
        if vehicle_name != nil{
            coder.encode(vehicle_name, forKey: "vehicle_name")
        }
        
        
    }
    
    required init?(coder: NSCoder) {
        id = coder.decodeObject(forKey: "id") as? Int
        vehicle_type = coder.decodeObject(forKey: "vehicle_type") as? String
        name = coder.decodeObject(forKey: "name") as? String
        email = coder.decodeObject(forKey: "email") as? String
        contact = coder.decodeObject(forKey: "contact") as? Int
        country_code = coder.decodeObject(forKey: "country_code") as? String
        profile = coder.decodeObject(forKey: "profile") as? String
        status = coder.decodeObject(forKey: "status") as? Int
        password = coder.decodeObject(forKey: "password") as? String
        otp = coder.decodeObject(forKey: "otp") as? Int
        vehicle_reg_no = coder.decodeObject(forKey: "vehicle_reg_no") as? String
        licence_no = coder.decodeObject(forKey: "licence_no") as? String
        bank_acc_no = coder.decodeObject(forKey: "bank_acc_no") as? String
        bank_name = coder.decodeObject(forKey: "bank_name") as? String
        acc_holder_name = coder.decodeObject(forKey: "acc_holder_name") as? String
        created_at = coder.decodeObject(forKey: "created_at") as? String
        updated_at = coder.decodeObject(forKey: "updated_at") as? String
        postalcode = coder.decodeObject(forKey: "postalcode") as? String
        address = coder.decodeObject(forKey: "address") as? String
        police_certificate = coder.decodeObject(forKey: "police_certificate") as? String
        token = coder.decodeObject(forKey: "token") as? String
        email_verified_at = coder.decodeObject(forKey: "email_verified_at") as? String
        vehicle_name = coder.decodeObject(forKey: "vehicle_name") as? String
    }
    
}
