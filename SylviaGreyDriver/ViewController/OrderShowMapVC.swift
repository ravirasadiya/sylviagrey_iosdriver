//
//  OrderShowMapVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 02/01/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import SocketIO



class OrderShowMapVC: UIViewController {
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var lblPickUpDateAndTime: UILabel!
    @IBOutlet weak var lblDropOutDateAndTime: UILabel!
    @IBOutlet var mapView: GMSMapView!
    
    //mark:- variable
    var locationManager = CLLocationManager()
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    var name:String? = ""
    var location:String? = ""
    var pickupDateTime:String? = ""
    var DropOutDateTime:String? = ""
    var latitudeData :String? = ""
    var LongitudeData :String? = ""
    var customerData = UserDefaults.standard.array(forKey: "CustomerData") as? [[String:Any]]
    var customer_Name:String? = ""
    var c_Latitude:Double?
    var c_longitude:Double?
    var mapApiResponse = [JSON]()
    //GameStarKids : http://18.221.146.27:8000/
    
    let manager = SocketManager(socketURL: URL(string: Socket_URL)!,config: [.log(true), .reconnectAttempts(-1), .reconnectWait(5), .reconnects(true),.version(.three)])

//    var socket:SocketIOClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        lblName.text = name
        lblLocation.text = location
        lblPickUpDateAndTime.text = pickupDateTime
        lblDropOutDateAndTime.text = DropOutDateTime
        //locationManager and request access to the user’s location.
        locationManager.delegate = self
      //  locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        SocketIOManager.sharedInstance.socket.connect()
        customerLocation(address: "Sharanam 10 Apartments,Anand Nagar, Prahlad Nagar, Ahmedabad, Gujarat 380015") { (location) in
            print(location.coordinate)
        }
        setuplocation()
    }
    override func viewWillAppear(_ animated: Bool) {
        setuplocation()
    }
    func setuplocation(){  //origin=23.014101,72.577624
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=23.000302,72.521177&destination=23.018434,72.558106&mode=driving&key=AIzaSyBsE4wyfrtrxtueWzJjDMu3lKynxI07o_E"
        self.apicalling(url: url) { (response) in
            // MARK: Marker for source location
           // if response.count != 0 {
            for data in response {
                let legs = data["legs"].array
                let sourceaddress = legs?[0]["start_address"].string
                let destaddress = legs?[0]["end_address"].string
                let sourcelat = legs?[0]["start_location"].dictionary
                let deslat = legs?[0]["end_location"].dictionary
                let sLat = Double((sourcelat?["lat"]?.double ?? 0))
                let sLng =  Double((sourcelat?["lng"]?.double ?? 0))
                let dLat =  Double((deslat?["lat"]?.double ?? 0))
                let dLng =  Double((deslat?["lng"]?.double ?? 0))
                self.c_longitude = sLng
                self.c_Latitude = sLat
                let soreceLocation = "\(sLat),\(sLng)"
                print("soreceLocation:\(soreceLocation)")
                print("destinationLocation:\(dLat),\(dLng)")
                let sourceMarker = GMSMarker()
                sourceMarker.position = CLLocationCoordinate2D(latitude: 23.000302, longitude: 72.521177)

                    //CLLocationCoordinate2D(latitude: sLat,longitude:sLng)
                
                sourceMarker.title = sourceaddress
                sourceMarker.snippet = sourceaddress
                sourceMarker.map = self.mapView
                
                // MARK: Marker for destination location
                let destinationMarker = GMSMarker()
                //destinationMarker.
                destinationMarker.position = CLLocationCoordinate2D(latitude: dLat , longitude: dLng)
                destinationMarker.title = destaddress
                destinationMarker.snippet = destaddress
                destinationMarker.map = self.mapView
                
                let camera = GMSCameraPosition(target: sourceMarker.position, zoom: 15)
                self.mapView.animate(to: camera)
            }
        }
    }
    func apicalling(url:String,completion: @escaping( _ responseData:[JSON]) -> Void) {
        // MARK: Request for response from the google
        print(url)
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (reseponse) in
            print("Response:\(reseponse)")
          
            guard let data = reseponse.data else {
                return
            }
            do {
                let jsonData = try JSON(data: data)
                print("jsonData:\(jsonData)")
                let routes = jsonData["routes"].arrayValue
                print("Routes:\(routes)")
                self.mapApiResponse = routes
                for route in routes {
                    let overview_polyline = route["overview_polyline"].dictionary
                    print("overview_polyline\(overview_polyline)")
                    let points = overview_polyline?["points"]?.string
                    print("points:\(points)")
                    let path = GMSPath.init(fromEncodedPath: points ?? "")
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeColor = UIColor.blue
                    polyline.strokeWidth = 5
                    polyline.map = self.mapView
                }
                completion(self.mapApiResponse)
            }
            catch let error {
                print(error.localizedDescription)
                return
            }
        }
      
    }
    //completion: @escaping(coordinateData:[String]) -> Void
    func customerLocation(address:String,completion: @escaping(_ coordinateData:CLLocation) -> Void) {
        let address = "380051,India"
        //"380015,India"
            "Sharanam 10 Apartments,Anand Nagar, Prahlad Nagar, Ahmedabad, Gujarat 380015,India"
       // let addresses = "1 Infinite Loop, CA, USA"
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
           if((error) != nil){
              print("Error", error)
           }
           if let placemark = placemarks?.first {
              let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                print("Address:\(coordinates)")
              }
            })
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //Mark:- Action
    @IBAction func btnBackPressed(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPaymentOnClick(_ sender: UIButton){
    }
    //MARK:Struct
  
    //MARK:Functions

    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
    func datetime()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = Date()
        let dateString = formatter.string(from:now)
        return dateString
    }
}


// MARK: - CLLocationManagerDelegate
//1 You create a MapViewController extension that conforms to CLLocationManagerDelegate.
extension OrderShowMapVC: CLLocationManagerDelegate {
  // 2 locationManager(_:didChangeAuthorization:) is called when the user grants or revokes location permissions.
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // 3 Here you verify the user has granted you permission while the app is in use.
    guard status == .authorizedWhenInUse else {
      return
    }
    // 4 Once permissions have been established, ask the location manager for updates on the user’s location.
    locationManager.startUpdatingLocation()
      
    //5 GMSMapView has two features concerning the user’s location: myLocationEnabled draws a light blue dot where the user is located, while myLocationButton, when set to true, adds a button to the map that, when tapped, centers the map on the user’s location.
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
  }
  
  // 6 locationManager(_:didUpdateLocations:) executes when the location manager receives new location data.


    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let accuratelocation = locations.last?.horizontalAccuracy
    
        let userLocation = locations.last
        
            let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)

        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude,
                longitude: userLocation!.coordinate.longitude, zoom: 8)
        GMSCameraUpdate.setTarget(center,zoom: 15)
        mapView.isMyLocationEnabled = true
            self.view = mapView

            let marker = GMSMarker()
            marker.position = center
            marker.title = "Current Location"
           // marker.snippet = "XXX"
            marker.map = mapView

            locationManager.stopUpdatingLocation()
        //socket emit
           // let status = "on"
            let datetimeString =  datetime()
            let lat = "\(center.latitude)"
            let lng = "\(center.longitude)"
        
            var param = [String:Any]()
            param["latitude"] = "\(center.latitude)"
            param["longitude"] = "\(center.longitude)"
            param["driver_id"] = "5"//"\(String(drive_id))"
            param["status"] = "on"
            param["time"] = datetimeString
            print(param)
            print("\(SocketIOManager.sharedInstance.socket.status)")
        if SocketIOManager.sharedInstance.socket.status == .connected {
            SocketIOManager.sharedInstance.socket.emit("driver_status", param)
        }else{
            print("\(SocketIOManager.sharedInstance.socket.status)")
        }
        
        //        print("current position: \(newLocation.coordinate.longitude) , \(newLocation.coordinate.latitude)")
        GMSCameraUpdate.setTarget(center,zoom: 15)
        
        // 7  This updates the map’s camera to center around the user’s current location. The GMSCameraPosition class aggregates all camera position parameters and passes them to the map for display.
     
        
        // 8 Tell locationManager you’re no longer interested in updates; you don’t want to follow a user around as their initial location is enough for you to work with.
        locationManager.stopUpdatingLocation()
    }

    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
      //  mapView.camera = GMSCameraPosition(latitude: 23.0, longitude: 72.0, zoom: 6)
    }
}


