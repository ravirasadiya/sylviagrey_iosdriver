//
//  TomorrowTableViewCell.swift
//  SylviaGreyDriver
//
//  Created by Vvk on 15/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit

class TomorrowTableViewCell: UITableViewCell {

    //Mark:-Outlets
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UIView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var btnviewTop: UIButton!
    @IBOutlet weak var viewTop: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //Mark:-Action
    @IBAction func btnviewTopAction(_ sender: UIButton) {
    }
}
