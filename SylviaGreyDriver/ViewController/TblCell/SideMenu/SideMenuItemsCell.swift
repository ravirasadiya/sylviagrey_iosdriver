//
//  SideMenuItemsCell.swift
//  SylviaGreyDriver
//
//  Created by Shine on 26/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class SideMenuItemsCell: UITableViewCell {

    //MARK:OUTLETS
    @IBOutlet weak var viewItem: UIView!
    @IBOutlet weak var lblItemTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
