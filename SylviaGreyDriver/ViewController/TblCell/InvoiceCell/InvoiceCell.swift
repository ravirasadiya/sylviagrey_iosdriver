//
//  InvoiceCell.swift
//  SylviaGreyDriver
//
//  Created by Shine on 30/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class InvoiceCell: UITableViewCell {

    //MARK:Outlets
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var btnPickUpHistory: UIButton!
    @IBOutlet weak var imgviewArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
     
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:- Action
    @IBAction func btnPickUPHistory(_ sender: UIButton) {
    }
    
}
