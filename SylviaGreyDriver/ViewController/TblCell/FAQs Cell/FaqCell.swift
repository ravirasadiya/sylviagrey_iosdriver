//
//  FaqCell.swift
//  Royal Laundry
//
//  Created by Shine on 13/10/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class ExpandingTableViewCellContent {
    var title: String?
    var subtitle: String?
    var expanded: Bool

    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
        self.expanded = false
    }
}

class FaqCell: UITableViewCell {

    
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var lblQueFaq: UILabel!
    @IBOutlet weak var lblAnsFaq: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(content: ExpandingTableViewCellContent) {
        self.lblQueFaq.text = content.title
        self.lblAnsFaq.text = content.expanded ? content.subtitle : ""
    }
    
}
