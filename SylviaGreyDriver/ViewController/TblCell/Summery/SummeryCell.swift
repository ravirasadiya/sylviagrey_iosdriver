//
//  SummeryCell.swift
//  SylviaGreyDriver
//
//  Created by Shine on 30/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class SummeryCell: UITableViewCell {

    
    
    @IBOutlet weak var viewPerson: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewSummeryCell: UIView!
    @IBOutlet weak var lblProfile: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
