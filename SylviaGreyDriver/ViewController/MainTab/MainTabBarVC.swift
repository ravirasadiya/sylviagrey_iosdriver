//
//  MainTabBarVC.swift
//  SylviaGreyDriver
//
//  Created by Apple on 16/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit

class MainTabBarVC: UIViewController {

    var str_tabName = "New"
    
    @IBOutlet weak var imgInvoice: UIImageView!
    @IBOutlet weak var lblInvoice: UILabel!
    @IBOutlet weak var lblTomorrow: UILabel!
    @IBOutlet weak var imgTomorrow: UIImageView!
    @IBOutlet weak var lblNew: UILabel!
    @IBOutlet weak var lblToday: UILabel!
    @IBOutlet weak var imgToday: UIImageView!
    @IBOutlet weak var imgNew: UIImageView!
    @IBOutlet weak var viewBtnInvoice: UIView!
    @IBOutlet weak var viewBtnTomorrow: UIView!
    @IBOutlet weak var viewBtnToday: UIView!
  //  @IBOutlet weak var imgViewInvoice: UIImageView!
    @IBOutlet weak var viewBtnNew: UIView!
    @IBOutlet weak var viewMainTab: UIView!
    @IBOutlet weak var viewNew: UIView!
    @IBOutlet weak var btnInvoice: UIButton!
 //   @IBOutlet weak var imgViewTomorrow: UIImageView!
    @IBOutlet weak var btnTomorrow: UIButton!
   // @IBOutlet weak var imgViewToday: UIImageView!
    @IBOutlet weak var btnToday: UIButton!
  //  @IBOutlet weak var imgViewNew: UIImageView!
    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var viewInvoice: UIView!
    @IBOutlet weak var viewTomorrow: UIView!
    @IBOutlet weak var viewToday: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        viewMainTab.backgroundColor = kAppThemePrimaryLightColor
//        viewBtnNew.addTapGesture(tapNumber: 1, target: self, action: #selector(btnNewOnClick))
//        viewBtnToday.addTapGesture(tapNumber: 1, target: self, action: #selector(btnTodayOnClick))
//        viewBtnTomorrow.addTapGesture(tapNumber: 1, target: self, action: #selector(btnTomorrowOnClick))
//        viewBtnInvoice.addTapGesture(tapNumber: 1, target: self, action: #selector(btnInvoiceOnClick))
        tabNameCheck()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //Mark:- Function
    func tabNameCheck() {
        if str_tabName == "New"{
            viewNew.alpha = 1
          //  btnNew.setImage(#imageLiteral(resourceName: "New-Unselected"), for: UIControl.State.selected)
            imgNew.image = #imageLiteral(resourceName: "New-Selected")
            lblNew.textColor = kAppThemePrimaryLightColor
            viewBtnNew.backgroundColor = .white
            Hide()
        }else if str_tabName == "Today"{
            viewNew.alpha = 0
            imgNew.image = #imageLiteral(resourceName: "New-Selected")
            lblNew.textColor = kAppThemePrimaryLightColor
            viewBtnNew.backgroundColor = .white
            
            viewToday.alpha = 1
            imgToday.image = #imageLiteral(resourceName: "Tomorrow-Selected")
          //  btnToday.setImage(#imageLiteral(resourceName: "Tomorrow-Selected"), for: UIControl.State.normal)
            lblToday.textColor = kAppThemePrimaryLightColor
           // btnToday.setTitleColor(kAppThemePrimaryLightColor, for: UIControl.State.normal)
            viewBtnToday.backgroundColor = .white
            
            viewTomorrow.alpha = 0
           // btnTomorrow.setTitleColor(.white, for: UIControl.State.normal)
            imgTomorrow.image = #imageLiteral(resourceName: "Tomorrow-Unselected")
            lblTomorrow.textColor = .white
          //  btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Unselected"), for: UIControl.State.normal)
            viewBtnTomorrow.backgroundColor = kAppThemePrimaryLightColor
            
            viewInvoice.alpha = 0
          //  btnInvoice.setTitleColor(.white, for: UIControl.State.normal)
            imgInvoice.image = #imageLiteral(resourceName: "invoice")
            lblInvoice.textColor = .white
           // btnInvoice.setImage(#imageLiteral(resourceName: "invoice"), for: UIControl.State.normal)
            viewBtnInvoice.backgroundColor = kAppThemePrimaryLightColor
        }else if str_tabName == "Tomorrow"{
            viewNew.alpha = 0
            imgNew.image = #imageLiteral(resourceName: "New-Selected")
            lblNew.textColor = .white
            viewBtnNew.backgroundColor = .white
//            btnNew.setImage(#imageLiteral(resourceName: "New-Unselected"), for: UIControl.State.normal)
//            btnNew.setTitleColor(.white, for: UIControl.State.normal)
//            btnNew.backgroundColor = kAppThemePrimaryLightColor
            
            viewToday.alpha = 0
          //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
            lblToday.textColor = .white
            imgToday.image = #imageLiteral(resourceName: "Today-UnSelected")
           // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
            viewBtnToday.backgroundColor = kAppThemePrimaryLightColor
            
            viewTomorrow.alpha = 1
            imgTomorrow.image = #imageLiteral(resourceName: "Tomorrow-Selected")
           // btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Selected"), for: UIControl.State.normal)
            lblTomorrow.textColor = kAppThemePrimaryLightColor
        //    btnTomorrow.setTitleColor(kAppThemePrimaryLightColor, for: UIControl.State.normal)
            viewBtnTomorrow.backgroundColor = .white
            
            viewInvoice.alpha = 0
            imgInvoice.image = #imageLiteral(resourceName: "invoice")
            lblInvoice.textColor = .white
//            btnInvoice.setImage(#imageLiteral(resourceName: "invoice"), for: UIControl.State.normal)
//            btnInvoice.setTitleColor(.white, for: UIControl.State.normal)
            viewBtnInvoice.backgroundColor = kAppThemePrimaryLightColor
        }else if str_tabName == "Invoice"{
            viewNew.alpha = 0
            imgNew.image = #imageLiteral(resourceName: "New-Unselected")
            lblNew.textColor = .white
            viewBtnNew.backgroundColor = kAppThemePrimaryLightColor
//            btnNew.setImage(#imageLiteral(resourceName: "New-Unselected"), for: UIControl.State.normal)
//            btnNew.setTitleColor(.white, for: UIControl.State.normal)
//            btnNew.backgroundColor = kAppThemePrimaryLightColor
            
            viewToday.alpha = 0
          //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
            lblToday.textColor = .white
            imgToday.image = #imageLiteral(resourceName: "Today-UnSelected")
           // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
            viewBtnToday.backgroundColor = kAppThemePrimaryLightColor
            
            viewTomorrow.alpha = 0
            viewBtnTomorrow.backgroundColor = kAppThemePrimaryLightColor
            imgTomorrow.image = #imageLiteral(resourceName: "Today-UnSelected")
            lblTomorrow.textColor = .white
            
            viewInvoice.alpha = 1
            viewBtnInvoice.backgroundColor = .white
            imgInvoice.image = #imageLiteral(resourceName: "invoiceUnselected")
            lblInvoice.textColor = kAppThemePrimaryLightColor
            btnInvoice.backgroundColor = .clear
        }else{
            viewNew.alpha = 1
            imgNew.image = #imageLiteral(resourceName: "New-Selected")
            lblNew.textColor = kAppThemePrimaryLightColor
            viewBtnNew.backgroundColor = .white
            Hide()
        }
    }
    //MARK:- Actions
    @IBAction func btnNewOnClick(_ sender: UIButton) {
  //  @objc func btnNewOnClick() {
        viewNew.alpha = 1
      //  btnNew.setImage(#imageLiteral(resourceName: "New-Unselected"), for: UIControl.State.selected)
        imgNew.image = #imageLiteral(resourceName: "New-Selected")
        lblNew.textColor = kAppThemePrimaryLightColor
        viewBtnNew.backgroundColor = .white
        
       
        viewToday.alpha = 0
      //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
        lblToday.textColor = .white
        imgToday.image = #imageLiteral(resourceName: "Today-UnSelected")
       // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
        viewBtnToday.backgroundColor = kAppThemePrimaryLightColor
        
        viewTomorrow.alpha = 0
        viewBtnTomorrow.backgroundColor = kAppThemePrimaryLightColor
       // btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Unselected"), for: UIControl.State.normal)
        imgTomorrow.image = #imageLiteral(resourceName: "Today-UnSelected")
        lblTomorrow.textColor = .white
        
        viewInvoice.alpha = 0
        viewBtnInvoice.backgroundColor = kAppThemePrimaryLightColor
        imgInvoice.image = #imageLiteral(resourceName: "invoice")
        lblInvoice.textColor = .white
    }
    
    @IBAction func btnTodayOnClick(_ sender: UIButton) {
//    @objc func btnTodayOnClick() {
        viewNew.alpha = 0
        imgNew.image = #imageLiteral(resourceName: "New-Unselected")
        lblNew.textColor = .white
        viewBtnNew.backgroundColor = kAppThemePrimaryLightColor
        btnNew.backgroundColor = .clear
        
        viewToday.alpha = 1
      //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
        lblToday.textColor = kAppThemePrimaryLightColor
        imgToday.image = #imageLiteral(resourceName: "Today-Selected")
        btnToday.backgroundColor = .clear
       // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
        viewBtnToday.backgroundColor = .white
        
        viewTomorrow.alpha = 0
        viewBtnTomorrow.backgroundColor = kAppThemePrimaryLightColor
       // btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Unselected"), for: UIControl.State.normal)
        imgTomorrow.image = #imageLiteral(resourceName: "Today-UnSelected")
        lblTomorrow.textColor = .white
        
        viewInvoice.alpha = 0
        viewBtnInvoice.backgroundColor = kAppThemePrimaryLightColor
        imgInvoice.image = #imageLiteral(resourceName: "invoice")
        lblInvoice.textColor = .white
    }
   
     @IBAction func btnTomorrowOnClick(_ sender: UIButton) {
//    @objc func btnTomorrowOnClick() {
        viewNew.alpha = 0
        imgNew.image = #imageLiteral(resourceName: "New-Unselected")
        lblNew.textColor = .white
        viewBtnNew.backgroundColor = kAppThemePrimaryLightColor
        
        viewToday.alpha = 0
      //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
        lblToday.textColor = .white
        imgToday.image = #imageLiteral(resourceName: "Today-UnSelected")
       // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
        viewBtnToday.backgroundColor = kAppThemePrimaryLightColor
        
        viewTomorrow.alpha = 1
        viewBtnTomorrow.backgroundColor = .white
       // btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Unselected"), for: UIControl.State.normal)
        imgTomorrow.image = #imageLiteral(resourceName: "Today-Selected")
        lblTomorrow.textColor = kAppThemePrimaryLightColor
        btnTomorrow.backgroundColor = .clear
        
        viewInvoice.alpha = 0
        viewBtnInvoice.backgroundColor = kAppThemePrimaryLightColor
        imgInvoice.image = #imageLiteral(resourceName: "invoice")
        lblInvoice.textColor = .white
     }
    
    @IBAction func btnInvoiceOnClick(_ sender: UIButton) {
//    @objc func btnInvoiceOnClick() {
        viewNew.alpha = 0
        imgNew.image = #imageLiteral(resourceName: "New-Unselected")
        lblNew.textColor = .white
        viewBtnNew.backgroundColor = kAppThemePrimaryLightColor
        
        viewToday.alpha = 0
      //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
        lblToday.textColor = .white
        imgToday.image = #imageLiteral(resourceName: "Today-UnSelected")
       // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
        viewBtnToday.backgroundColor = kAppThemePrimaryLightColor
        
        viewTomorrow.alpha = 0
        viewBtnTomorrow.backgroundColor = kAppThemePrimaryLightColor
       // btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Unselected"), for: UIControl.State.normal)
        imgTomorrow.image = #imageLiteral(resourceName: "Today-UnSelected")
        lblTomorrow.textColor = .white
        
        viewInvoice.alpha = 1
        viewBtnInvoice.backgroundColor = .white
        imgInvoice.image = #imageLiteral(resourceName: "invoiceUnselected")
        lblInvoice.textColor = kAppThemePrimaryLightColor
        btnInvoice.backgroundColor = .clear
    }
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func Hide(){
        viewToday.alpha = 0
      //  btnToday.setTitleColor(.white, for: UIControl.State.normal)
        lblToday.textColor = .white
        imgToday.image = #imageLiteral(resourceName: "Today-UnSelected")
       // btnToday.setImage(#imageLiteral(resourceName: "Today-UnSelected"), for: UIControl.State.normal)
        viewBtnToday.backgroundColor = kAppThemePrimaryLightColor
        
        viewTomorrow.alpha = 0
        viewBtnTomorrow.backgroundColor = kAppThemePrimaryLightColor
       // btnTomorrow.setImage(#imageLiteral(resourceName: "Tomorrow-Unselected"), for: UIControl.State.normal)
        imgTomorrow.image = #imageLiteral(resourceName: "Today-UnSelected")
        lblTomorrow.textColor = .white
        
        viewInvoice.alpha = 0
        viewBtnInvoice.backgroundColor = kAppThemePrimaryLightColor
        imgInvoice.image = #imageLiteral(resourceName: "invoice")
        lblInvoice.textColor = .white
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}

