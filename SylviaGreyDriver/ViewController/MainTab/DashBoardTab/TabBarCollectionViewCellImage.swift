//
//  TabBarCollectionViewCellImage.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 17/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

public class TabBarCollectionViewCellImage: UICollectionViewCell {
    public static let resusableName = String(describing: TabBarCollectionViewCellImage.self)
    @IBOutlet public weak var imageView: UIImageView!
}
