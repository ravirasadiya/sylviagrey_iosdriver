//
//  TabBarCollectionViewCellTitleWithImage.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 09/11/20.
//  Copyright © 2020 test. All rights reserved.
//
//

import UIKit

public class TabBarCollectionViewCellTitleWithImage: UICollectionViewCell {
    public static let resusableName = String(describing: TabBarCollectionViewCellTitleWithImage.self)
    @IBOutlet public weak var imageView: UIImageView!
    @IBOutlet public weak var titleLabel: UILabel!
        
    
}
