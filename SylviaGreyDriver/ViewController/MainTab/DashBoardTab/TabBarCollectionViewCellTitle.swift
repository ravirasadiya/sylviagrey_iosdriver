//
//  TabBarCollectionViewCellTitle.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 09/11/20.
//  Copyright © 2020 test. All rights reserved.
//
import UIKit

public class TabBarCollectionViewCellTitle: UICollectionViewCell {
    @IBOutlet public weak var titleLabel: UILabel!
    public static let resusableName = String(describing: TabBarCollectionViewCellTitle.self)
}
