//
//  NSObjectExtensions.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 09/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

extension NSObject {
    class func fromClassName(className : String) -> AnyClass? {
        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + className
        let aClass = NSClassFromString(className)
        return aClass
    }
}
