//
//  TabBarCollectionViewCell.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 09/11/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class TabBarCollectionViewCell: UICollectionViewCell {
    static let resusableName = String(describing: TabBarCollectionViewCell.self)
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
