//
//  SlideViewControllerTitleWithImage.swift
//  DemoAPI
//
//  Created by Latitude Technolabs on 09/11/20.
//  Copyright © 2020 test. All rights reserved.
//
//

import UIKit

class SlideViewControllerTitleWithImage: UIViewController, TabBarDataSourse {
    //MARK:- Outlets

    // MARK: - constants & variables
    let viewControllerTabTitle = ["TOPS", "LAUNDRY", "BEDDING", "SUITS", "TROUSERS", "DRESSES", "OUTDOOR", "ACCESSORIES", "HOME", "BUSINESS"]
    let viewControllerTabImage = [ #imageLiteral(resourceName: "tops"),#imageLiteral(resourceName: "laundry"),#imageLiteral(resourceName: "bedding"),#imageLiteral(resourceName: "suits"),#imageLiteral(resourceName: "trousers"),#imageLiteral(resourceName: "dresses"),#imageLiteral(resourceName: "outdoor"),#imageLiteral(resourceName: "acessories"),#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "bussiness")]

    //MARK:- UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func cellTabName() -> String {
        return TabBarCollectionViewCellTitleWithImage.resusableName
    }
    
    func configureTabCell(collectionView: UICollectionView, indexpath:IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellTabName(), for: indexpath) as! TabBarCollectionViewCellTitleWithImage
        cell.imageView.image = viewControllerTabImage[indexpath.row]
        cell.titleLabel.text = "\(viewControllerTabTitle[indexpath.row])"
        return cell
    }

}

