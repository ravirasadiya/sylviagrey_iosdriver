//
//  ALLVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import  SVProgressHUD
class ALLVC: UIViewController {
    
    //MARK:Outlets
    @IBOutlet weak var tblTodayALL: UITableView!
    //Mark:-Variable
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    var customerData  = UserDefaults.standard.array(forKey: "CustomerData")  as? [[String:Any]]
    var all:[[String:Any]] = [[:]]
    var pendingData = [[:]]
    var responseMessage = ""
   // let obj = pendingRequestViewModel()
    var arrToday:[TodayOrderAll] = [TodayOrderAll]()
    var customer_Name:String?
    var customer_Id:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblTodayALL.isHidden = true
        customer_Name = customerData?[0]["name"] as? String ?? ""
        customer_Id = customerData?[0]["Customer_id"] as? Int ?? 0
        //Register Xib
        let nib = UINib(nibName: "TodayCell", bundle: nil)
        tblTodayALL.register(nib, forCellReuseIdentifier: "TodayCell")
        tblTodayALL.tableFooterView = UIView() // Removes empty cell separators
        tblTodayALL.separatorStyle = UITableViewCell.SeparatorStyle.none
       // tblTodayALL.layer.cornerRadius = 20.0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show()
        let obj = TodayOrderViewModel()
        obj.TodayorderRequest(driver_id: drive_id) { [self] (isSucess, TodayOrderData,message) in
            print(isSucess)
            SVProgressHUD.dismiss()
            print("TodayOrder = \(TodayOrderData)")
            if TodayOrderData.isEmpty != true {
                if message == "Order not found."{
                    responseMessage = message
                    
                    tblTodayALL.isHidden = true
                    tblTodayALL.reloadData()
                }else{
                    if message == "Orders not assign driver."{
                        responseMessage = message
                        tblTodayALL.isHidden = true
                        tblTodayALL.reloadData()
                        print("no data")
                        
                    }else{
                        let Tdata = TodayOrderData["data"] as! [String:Any]
                        all = Tdata["All"] as? [[String : Any]] ?? [[:]]
                        tblTodayALL.isHidden = false
                        tblTodayALL.reloadData()
                    }
                    
                }
            }else{
                
            }
        }
        
    }
    @objc  func btnPickUpHistory(sender : UIButton!) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        vc.modalPresentationStyle = .fullScreen
        vc.order_id = all[sender.tag]["id"] as? Int
        if responseMessage == "Orders not assign driver."{
            
        }else{
            if all[sender.tag]["order_status_id"] as? Int == 2 {
                vc.dropup = "dropup"
                 // vc.btnPickup.setTitle("Drop Up", for: UIControl.State.normal)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }

}
extension ALLVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseMessage == "Order not found."{
            return 0
        }else if responseMessage == "Orders not assign driver."{
            return 0
        } else{
            return all.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodayCell") as! TodayCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 20.0
        cell.contentView.cornerRadius = 20.0
        cell.viewCell.cornerRadius = 20.0
        cell.viewTop.cornerRadius = 20
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        if all.count != 0 {
            if all[indexPath.row]["order_status_id"] as? Int == 2 {
              
                cell.btnPickUpHistory.backgroundColor = UIColor.darkGray
               // cell.btnPickUpHistory.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: UIControl.State.normal)
                cell.btnPickUpHistory.tag = indexPath.row
                cell.btnPickUpHistory.addTarget(self, action: #selector(btnPickUpHistory(sender:)), for: UIControl.Event.touchUpInside)
                cell.imgviewArrow.image = #imageLiteral(resourceName: "down")
                cell.viewTop.backgroundColor = kAppButtonBGDeactivateColor
                cell.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
            }else{
                cell.btnPickUpHistory.tag = indexPath.row
                cell.btnPickUpHistory.addTarget(self, action: #selector(btnPickUpHistory(sender:)), for: UIControl.Event.touchUpInside)
            }
            cell.viewTop.backgroundColor = kAppThemePrimaryLightColor
            cell.lblLocation.text = all[indexPath.row]["address"] as? String//all["address"] as? String
            cell.lblDate.text = all[indexPath.row]["collection_date"] as? String//all["collection_date"] as? String
            let id = all[indexPath.row]["id"] as? Int
            if id != nil{
                cell.lblTitle.text = "\(String(describing:id!))"
                cell.lblName.text = customer_Name
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 189
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        vc.modalPresentationStyle = .fullScreen
        vc.order_id = all[indexPath.row]["id"] as? Int
        if responseMessage == "Orders not assign driver."{
            
        }else{
            if all[indexPath.row]["order_status_id"] as? Int == 2 {
                vc.dropup = "dropup"
                 // vc.btnPickup.setTitle("Drop Up", for: UIControl.State.normal)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
}
