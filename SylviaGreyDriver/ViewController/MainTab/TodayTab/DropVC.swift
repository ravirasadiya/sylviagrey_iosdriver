//
//  DropVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class DropVC: UIViewController {
    
    @IBOutlet weak var tblviewDrop: UITableView!
    
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    var responseDropData:[[String:Any]] = [[:]]
    var responseMessage:String?
    
    var customerData = UserDefaults.standard.array(forKey: "CustomerData") as? [[String:Any]]
    var customer_Name:String?
    var customer_Id:Int?
    var order_id:Int?
    
    var all:[[String:Any]] = [[:]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customer_Name = customerData?[0]["name"] as? String ?? ""
        customer_Id = customerData?[0]["Customer_id"] as? Int ?? 0
        order_id = customerData?[0]["id"] as? Int
        //        order_id = 0
        //Register Xib
        let nib = UINib(nibName: "TodayCell", bundle: nil)
        tblviewDrop.register(nib, forCellReuseIdentifier: "TodayCell")
        tblviewDrop.tableFooterView = UIView() // Removes empty cell separators
        tblviewDrop.separatorStyle = UITableViewCell.SeparatorStyle.none
      //  tblviewDrop.layer.cornerRadius = 20.0
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show()
        let obj = TodayOrderViewModel()
        obj.TodayorderRequest(driver_id: drive_id) { [self] (isSucess, TodayOrderData,message) in
            print(isSucess)
            SVProgressHUD.dismiss()
            print("TodayOrder = \(TodayOrderData)")
            if TodayOrderData.isEmpty != true {
                if message == "Order not found."{
                    responseMessage = message
                    tblviewDrop.reloadData()
                }else if message == "Orders not assign driver."{
                    responseMessage = message
                    tblviewDrop.reloadData()
                }else{
                    let Tdata = TodayOrderData["data"] as! [String:Any]
                    all = Tdata["Delivery"] as? [[String : Any]] ?? [[:]]
                    tblviewDrop.reloadData()
                }
            }
        }
    }
    
    @objc func btnDropHistory(sender : UIButton!) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        vc.modalPresentationStyle = .fullScreen
        vc.order_id = order_id
        vc.dropup = "dropup"
        self.present(vc, animated: true, completion: nil)
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension DropVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseMessage == "Orders not found."{
            return 0
        }else if responseMessage == "Orders not assign driver."{
            return 0
        }else{
            return all.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodayCell") as! TodayCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 20.0
        cell.contentView.cornerRadius = 20.0
        cell.viewCell.cornerRadius = 20.0
        cell.viewTop.cornerRadius = 20
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.imgviewArrow.image = #imageLiteral(resourceName: "down")
        cell.btnPickUpHistory.tag = indexPath.row
        cell.btnPickUpHistory.addTarget(self, action: #selector(btnDropHistory(sender:)), for: UIControl.Event.touchUpInside)
        cell.viewTop.backgroundColor = kAppButtonBGDeactivateColor
        cell.lblTitle.textColor = .white
        if all.count != 0 {
            if all[indexPath.row]["order_status_id"] as? Int == 2 {
                cell.lblLocation.text = all[indexPath.row]["address"] as? String
                cell.lblDate.text = all[indexPath.row]["delivery_date"] as? String
                let id = all[indexPath.row]["id"] as? Int
                if id != nil{
                    cell.lblTitle.text = "\(String(describing:id!))"
                    cell.lblName.text = customer_Name
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 189
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        vc.modalPresentationStyle = .fullScreen
        vc.order_id = order_id
        vc.dropup = "dropup"
        self.present(vc, animated: true, completion: nil)
    }
}

