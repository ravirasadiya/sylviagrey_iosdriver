//
//  TodayVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class TodayVC: UIViewController {
    
    //MARK:Outlets
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewSegment: ScrollableSegmentedControl!
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var viewPick: UIView!
    @IBOutlet weak var viewDrop: UIView!
    
    //ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        //segment
        setNeedsStatusBarAppearanceUpdate()
        viewNavigation.backgroundColor = kAppThemePrimaryLightColor
        statusbar()
        viewSegment.segmentStyle = .textOnly
        viewSegment.insertSegment(withTitle: "ALL",  at: 0)
        viewSegment.insertSegment(withTitle: "PICK",  at: 1)
        viewSegment.insertSegment(withTitle: "DROP",  at: 2)
        viewSegment.underlineSelected = true
        viewSegment.segmentContentColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let size = UIFont.boldSystemFont(ofSize: 15)
        viewSegment.setTitleTextAttributes([NSAttributedString.Key.font : size ], for: .normal)
        viewSegment.selectedSegmentIndex = 0
        viewSegment.selectedSegmentContentColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        viewSegment.backgroundColor = kAppThemePrimaryLightColor
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func viewSegmentTap(_ sender: ScrollableSegmentedControl) {
        
        if sender.selectedSegmentIndex == 0{
            viewAll.alpha = 1
            viewPick.alpha = 0
            viewDrop.alpha = 0
        }else if sender.selectedSegmentIndex == 1{
            viewAll.alpha = 0
            viewPick.alpha = 1
            viewDrop.alpha = 0
        }else{
            viewAll.alpha = 0
            viewPick.alpha = 0
            viewDrop.alpha = 1
            
        }
    }
    
    @IBAction func btnSideMenuPressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: true, completion: nil)
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
