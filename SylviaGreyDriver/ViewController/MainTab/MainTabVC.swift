//
//  MainTabVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class MainTabVC: UITabBarController {
    //MARK:Outlets
    @IBOutlet weak var viewTabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        //set tabbar items
        setTabBarItems()
        setNeedsStatusBarAppearanceUpdate()
        //set default tab seting
        //viewTabBar
        //change color
        UITabBar.appearance().barTintColor = kAppThemePrimaryLightColor
        viewTabBar.backgroundColor = kAppThemePrimaryLightColor
        viewTabBar.unselectedItemTintColor = .white
        self.viewTabBar.itemPositioning = .fill
//        viewTabBar.frame.origin.x = -2
//        viewTabBar.frame.size.width += 4
        self.viewTabBar.itemSpacing = 0
        // set red as selected background color
        viewTabBar.borderWidth = 0
       // viewTabBar.borderColor = .clear
        let numberOfItems = CGFloat(viewTabBar.items!.count)
        
        let tabBarItemSize = CGSize(width:((UIScreen.main.bounds.width) / numberOfItems), height: viewTabBar.frame.height) //35
     
        viewTabBar.selectionIndicatorImage = UIImage(named: "DashUnSelected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        viewTabBar.selectionIndicatorImage = UIImage.imageWithColor(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0
        ))
  
        for item in viewTabBar.items! {
            item.imageInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        // remove default border
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0, green: 0.6863893867, blue: 0.68882972, alpha: 1)], for: .selected)
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK:Functions
    func setTabBarItems(){
        
        let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
        
        let normalFontStyle = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight(rawValue: 0))
        myTabBarItem1.image = UIImage(named: "New-Unselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.selectedImage = UIImage(named: "New-Selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.setTitleTextAttributes(
            [NSAttributedString.Key.font:normalFontStyle], for: UIControl.State.normal)
        myTabBarItem1.title = "New"
//        myTabBarItem1.imageInsets = UIEdgeInsets(top: 0, left: -0.6, bottom: 0, right: 0)
        
        let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
        myTabBarItem2.image = UIImage(named: "Today-UnSelected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.selectedImage = UIImage(named: "Today-Selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.setTitleTextAttributes(
            [NSAttributedString.Key.font:normalFontStyle], for: UIControl.State.normal)
        myTabBarItem2.title = "Today"
//        myTabBarItem2.imageInsets = UIEdgeInsets(top: 2, left: 0, bottom: -2, right: 0)
        
        
        let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
        //  myTabBarItem3.image = #imageLiteral(resourceName: "Tomorrow-Unselected")
        myTabBarItem3.image = UIImage(named: "Today-UnSelected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = UIImage(named: "Today-Selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        //  myTabBarItem3.t = UIColor.darkGray
        myTabBarItem3.setTitleTextAttributes(
            [NSAttributedString.Key.font:normalFontStyle], for: UIControl.State.normal)
        myTabBarItem3.title = "Tomorrow"
//        myTabBarItem3.imageInsets = UIEdgeInsets(top: 2, left: 0, bottom: -2, right: 0)
        
        let myTabBarItem4 = (self.tabBar.items?[3])! as UITabBarItem
        // myTabBarItem4.image = #imageLiteral(resourceName: "Invoice -Unselected")
        //invoice //invoiceUnselected
        myTabBarItem4.image = UIImage(named: "invoice")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.selectedImage = UIImage(named: "invoiceUnselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.setTitleTextAttributes(
            [NSAttributedString.Key.font:normalFontStyle], for: UIControl.State.normal)
        
        myTabBarItem4.title = "Invoice"
//        myTabBarItem4.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
