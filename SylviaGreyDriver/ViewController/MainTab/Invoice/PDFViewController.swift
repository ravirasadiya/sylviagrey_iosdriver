//
//  PDFViewController.swift
//  SylviaGreyDriver
//
//  Created by Apple on 27/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
class PDFViewController: UIViewController {

    //Mark:-outlets
    
    @IBOutlet weak var viewShow: UIView!
    
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewnavigation: UIView!
    var urlstring = ""
    var order_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        viewnavigation.backgroundColor = kAppThemePrimaryLightColor
        statusbar()
        SVProgressHUD.show()
        viewShow.backgroundColor = .clear
        lblOrderID.text = "Order \(order_id)"
        self.displayWebView()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private func displayWebView() {
        if let webView = self.createWebView(withFrame: viewShow.bounds) {
            self.viewShow.addSubview(webView)
            webView.backgroundColor = .clear
        }
    }
    private func createWebView(withFrame frame: CGRect) -> WKWebView? {
        SVProgressHUD.show()
        let webView = WKWebView(frame: frame)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if let resourceUrl = URL(string: urlstring) {
          
               let request = URLRequest(url: resourceUrl)
                SVProgressHUD.dismiss()
                webView.load(request)
                SVProgressHUD.dismiss()
               return webView
           }
        
        return nil
    }

    @IBAction func btnBackOnClick(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
