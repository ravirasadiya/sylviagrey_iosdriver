//
//  InvioceDetailsViewController.swift
//  SylviaGreyDriver
//
//  Created by Apple on 27/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import WebKit

class InvioceDetailsViewController: UIViewController {
    
    //Mark:-outlets
    @IBOutlet weak var viewnavigation: UIView!
    @IBOutlet weak var tblviewInvoiceItem: UITableView!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var btnPDF: UIButton!
    
    @IBOutlet weak var btnSideMenu: UIButton!
    var Data = [[String:Any]]()
    var order_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        viewnavigation.backgroundColor = kAppThemePrimaryLightColor
        btnPDF.backgroundColor = kAppThemePrimaryLightColor
        print(Data)
        if Data.isEmpty == false {
            lblName.text = Data[0]["name"] as? String
            lblLocation.text = Data[0]["address"] as? String
            lblOrderID.text = "Order \(Data[0]["order_id"] as! Int)"
            lblDateTime.text = "\(Data[0]["collection_date"] as! String)   \(Data[0]["collection_time"] as! String)"
        }
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLayoutSubviews() {
        //tableview height Setup
        tblHeightConstraint.constant = tblviewInvoiceItem.contentSize.height
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //Mark:-Action
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.str_tabName = "Invoice"
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: false, completion: nil)
//        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnPDFOnClick(_ sender: UIButton) {
        let invoicepath = Data[0]["invoice_path"] as! String
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        vc.modalPresentationStyle = .fullScreen
        if order_id != "" {
            vc.order_id = order_id //"\(Data[0]["order_id"] as! Int)"//Data[]["order_id"] as? Int
        }
        vc.urlstring = invoicepath
        self.present(vc, animated: false, completion: nil)
        //        let application = UIApplication.shared
        //        if application.canOpenURL(url! as URL){
        //            application.open(url!, options: [:], completionHandler: nil)
        //        }
    }
    func statusbar(){
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = kAppThemePrimaryDarkColor
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = kAppThemePrimaryDarkColor
        }
    }
    
}
extension InvioceDetailsViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Data.isEmpty == false {
            return Data.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let itemdata = Data[indexPath.row]["item"] as? [[String:Any]]
        order_id = "\(Data[indexPath.row]["order_id"] as! Int)"
        if Data.count != 0 {
            cell!.textLabel?.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            cell!.detailTextLabel?.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            cell?.textLabel?.adjustsFontSizeToFitWidth = true
            cell?.textLabel?.font = cell?.textLabel?.font.withSize(12)
            cell?.detailTextLabel?.adjustsFontSizeToFitWidth = true
            cell?.detailTextLabel?.font = cell?.textLabel?.font.withSize(12)
            cell!.textLabel?.numberOfLines = 3
            cell?.textLabel?.text =  "1x  \(itemdata![indexPath.row]["product_name"] as! String)"
            cell?.detailTextLabel?.text = "£\(itemdata![indexPath.row]["product_price"] as! String)"
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

