//
//  TomorrowVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class TomorrowVC: UIViewController {
    //Mark:- Outlets
    @IBOutlet weak var lblDatanotFound: UILabel!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tblTomorrow: UITableView!
    //    @IBOutlet weak var viewDropOut: UIView!
//    @IBOutlet weak var viewpickUp: UIView!
//    @IBOutlet weak var viewScroll: ScrollableSegmentedControl!
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        SVProgressHUD.show()
        lblDatanotFound.isHidden = true
        tblTomorrow.isHidden = true
        viewNavigation.backgroundColor = kAppThemePrimaryLightColor
        //Register Xib
        let nib = UINib(nibName: "TodayCell", bundle: nil)
        tblTomorrow.register(nib, forCellReuseIdentifier: "TodayCell")
        tblTomorrow.tableFooterView = UIView() // Removes empty cell separators
        tblTomorrow.separatorStyle = UITableViewCell.SeparatorStyle.none
        //segment
//        viewScroll.segmentStyle = .textOnly
//        viewScroll.insertSegment(withTitle: "PICK",  at: 0)
//        viewScroll.insertSegment(withTitle: "DROP",  at: 1)
//        viewScroll.underlineSelected = true
//        viewScroll.segmentContentColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        let size = UIFont.boldSystemFont(ofSize: 15)
//        viewScroll.setTitleTextAttributes([NSAttributedString.Key.font : size ], for: .normal)
//        viewScroll.selectedSegmentIndex = 0
//        viewScroll.selectedSegmentContentColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        viewScroll.backgroundColor = kAppThemePrimaryLightColor
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let obj = TomorrowViewModel()
        obj.TomorroworderRequest(driver_id: drive_id) { [self] (isSucess, data,message)  in
            SVProgressHUD.dismiss()
            print(isSucess)
            lblDatanotFound.isHidden = false
            tblTomorrow.isHidden = true
            print("Tomorrow Response = \(data)")
            tblTomorrow.reloadData()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //Mark:- Action
//    @IBAction func viewScrollableSegmentTap(_ sender: ScrollableSegmentedControl) {
//
////        if sender.selectedSegmentIndex == 0{
////            viewpickUp.alpha = 1
////            viewDropOut.alpha = 0
////        }else{
////            viewpickUp.alpha = 0
////            viewDropOut.alpha = 1
////        }
//    }
    @IBAction func btnSideMenuPressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: true, completion: nil)
        //self.presentInFullScreen(objVC, animated: true)
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}

extension TomorrowVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodayCell") as! TodayCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 20.0
        cell.contentView.cornerRadius = 20.0
        cell.viewCell.cornerRadius = 20.0
        cell.viewTop.cornerRadius = 20
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 189
    }
    
}
