//
//  PickUpViewController.swift
//  SylviaGreyDriver
//
//  Created by Vvk on 15/02/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class PickUpViewController: UIViewController {
    //Mark:-outlets
    
    @IBOutlet weak var tblviewPickUp: UITableView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //tableview
        tblviewPickUp.isHidden = true
        //Register Xib
        let nib = UINib(nibName: "TomorrowTableViewCell", bundle: nil)
        tblviewPickUp.register(nib, forCellReuseIdentifier: "TomorrowTableViewCell")
        tblviewPickUp.tableFooterView = UIView() // Removes empty cell separators
        tblviewPickUp.separatorStyle = UITableViewCell.SeparatorStyle.none
       // tblviewPickUp.layer.cornerRadius = 20.0
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
      
    }
    @objc func btnPickUpHistory(sender : UIButton!) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
extension PickUpViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TomorrowTableViewCell") as! TomorrowTableViewCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 20.0
        cell.contentView.cornerRadius = 20.0
        cell.viewCell.cornerRadius = 20.0
        cell.viewTop.cornerRadius = 20
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.layer.shadowRadius = 12.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.backgroundColor = kAppThemePrimaryLightColor
      //  cell.btnviewTop.addTarget(self, action: #selector(btnPickUpHistory(sender:)), for: UIControl.Event.touchUpInside)
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 189
        
    }
}
