//
//  DashBoardVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class DashBoardVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var viewnavigationbar: UIView!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var imgviewPerson: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var viewDropOutData: UIView!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var viewPickUpData: UIView!
    @IBOutlet weak var viewSeperater: UIView!
    @IBOutlet weak var imgviewLocation: UIImageView!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var imgviewUpArrow: UIImageView!
    @IBOutlet weak var viewCircularProgressbar: CircularProgressBar!
    @IBOutlet weak var imgviewDownArrow: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPickUpTime: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var lblDropOutTime: UILabel!
    @IBOutlet weak var lblDropOutDate: UILabel!
    
    @IBOutlet weak var lblordernotFound: UILabel!
    var arrData = UserDefaults.standard.array(forKey: "OrderDataDict")
    var arr:[String:Any] = [String:Any]()
    //    var acceptOrderRequestData:[:] = [:]
    
    //ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        viewnavigationbar.backgroundColor = kAppThemePrimaryLightColor
        lblordernotFound.isHidden = true
        hide()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleAppDidBecomeActiveNotification(notification:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
        viewCircularProgressbar.safePercent = 100
        viewCircularProgressbar.setProgress(to: 1, withAnimation: false)
        viewCircularProgressbar.addTapGesture(tapNumber: 1, target: self, action: #selector(viewAcceptProgressbarTap))
    }
    @objc func handleAppDidBecomeActiveNotification(notification: Notification) {
        viewWillAppear(true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show()
        let DriverId = UserDefaults.standard.integer(forKey: "driverID")
        let obj = pendingRequestViewModel()
       
        if DriverId != 0 {
            obj.PendingRequest(driver_id: DriverId) { [self] (isSucess,statusCode,data,message) in
                print("Done")
                if statusCode != 0 {
                    if message == "Order not found." {
                        SVProgressHUD.dismiss()
                        hide()
                        lblordernotFound.isHidden = false
                    }else{
                        SVProgressHUD.dismiss()
                        show()
                        lblordernotFound.isHidden = true
                        self.arr = data[0]
                        let name = data[0]["name"] as! String
                        let Customer_id = data[0]["id"] as! Int
                        let order_id = data[0]["id"] as! Int
                        let CustomerData = [["name":name,"Customer_id":Customer_id,"id":order_id]] as [[String:Any]]
                        UserDefaults.standard.setValue(CustomerData, forKey: "CustomerData")
                        self.lblName.text = data[0]["name"] as! String
                        self.lblAddress.text = data[0]["address"] as! String
                        self.lblPickUpDate.text = data[0]["collection_date"] as! String
                        self.lblPickUpTime.text = data[0]["collection_time"] as! String
                        self.lblDropOutDate.text = data[0]["delivery_date"] as! String
                        self.lblDropOutTime.text = data[0]["delivery_time"] as! String
                        if data[0]["order_status_id"] as! Int == 6 {
                            lblOrderStatus.text = "Order pickUp"
                        }else if data[0]["order_status_id"] as! Int == 2 {
                            lblOrderStatus.text = "Order Drop"
                        }else if data[0]["order_status_id"] as! Int == 1 {
                            lblOrderStatus.text = "Order Deliver"
                        }else if data[0]["order_status_id"] as! Int == 3 {
                            lblOrderStatus.text = "Order pickUp"
                        }
                    }
                }else{
                    SVProgressHUD.dismiss()
                    hide()
                    lblordernotFound.isHidden = false
                }
            }
        }
    }
    //MARK: Function
    @objc func viewAcceptProgressbarTap(){
//        if arr["order_status_id"] as? Int != 0{
//
//        }
        SVProgressHUD.show()
        let obj = acceptorderRequestViewModel()
        //        let data =  obj.acceptorderRequest(Order_id: arr["id"] as! Int)
        //        let responsecode = data[0]["responseCode"] as! Int
        //        print("response data of acceptOrder  =\(data[1])")
        //        print(responsecode)
        obj.acceptorderRequest(Order_id: arr["id"] as! Int) { [self] (isSucess,statusCode ,Data)  in
          
            if isSucess {
                if statusCode != 0 {
                    SVProgressHUD.dismiss()
                    print("response data of accept Order=\(Data)")
                    viewWillAppear(true)
                }
            }else{
                SVProgressHUD.dismiss()
                print("Not Success")
            }
        }
    }
    func hide() {
        viewCircularProgressbar.isHidden = true
        lblName.isHidden = true
        viewDropOutData.isHidden = true
        lblDropOutDate.isHidden = true
        lblDropOutTime.isHidden = true
        lblAddress.isHidden = true
        viewPickUpData.isHidden = true
        imgviewDownArrow.isHidden = true
        imgviewUpArrow.isHidden = true
        lblPickUpTime.isHidden = true
        lblPickUpDate.isHidden = true
        viewSeperater.isHidden = true
        imgviewPerson.isHidden = true
        imgviewLocation.isHidden = true
        btnDecline.isHidden = true
        lbl2.isHidden = true
        lbl1.isHidden = true
        lblOrderStatus.isHidden = true
    }
    func show() {
        viewCircularProgressbar.isHidden = false
        lblName.isHidden = false
        viewDropOutData.isHidden = false
        lblDropOutDate.isHidden = false
        lblDropOutTime.isHidden = false
        lblAddress.isHidden = false
        viewPickUpData.isHidden = false
        imgviewDownArrow.isHidden = false
        imgviewUpArrow.isHidden = false
        lblPickUpTime.isHidden = false
        lblPickUpDate.isHidden = false
        viewSeperater.isHidden = false
        imgviewPerson.isHidden = false
        imgviewLocation.isHidden = false
        btnDecline.isHidden = false
        lbl2.isHidden = false
        lbl1.isHidden = false
        lblOrderStatus.isHidden = false
    }
    //MARK:Actions
    @IBAction func btnSideMenuPressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        self.presentInFullScreen(objVC, animated: true)
    }
    @IBAction func btnDecline(_ sender: UIButton) {
        SVProgressHUD.show()
        let obj  = declineorderRequestViewModel()
        obj.declineorder(order_id: arr["id"] as! String) { (isSuccess) in
            if isSuccess {
                SVProgressHUD.dismiss()
                print("success")
                self.lblName.text = ""
                self.lblAddress.text = ""
                self.lblPickUpTime.text = ""
                self.lblPickUpDate.text = ""
                self.lblDropOutTime.text = ""
                self.lblDropOutDate.text = ""
                self.arr.removeAll()
                self.arrData?.remove(at: 1)
            }else{
                SVProgressHUD.dismiss()
                print("Not Done")
            }
        }
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
