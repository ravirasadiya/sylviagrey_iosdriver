//
//  OTPVerifyVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class OTPVerifyVC: UIViewController {
    
    // MARK: - constants & variables
    var remainnigSecond = 0
    var objTimer: Timer?
    var strMobileNumber = ""
    var strOtp:String = ""
    var strNotificationToken = ""
    var strCountry_Code = ""
    var strType = ""
    var strtoken = ""
    //MARK:Outlets
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var txtSix: UITextField!
    @IBOutlet weak var txtFive: UITextField!
    @IBOutlet weak var txtFourth: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtOne: UITextField!
    @IBOutlet weak var viewStatusbar: UIView!
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var btnheightConstraints: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
          self.navigationController?.navigationBar.isHidden = false
        btnheightConstraints.constant = 0
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        viewStatusbar.backgroundColor = .clear//kAppThemePrimaryDarkColor
        viewBackground.backgroundColor = kAppThemePrimaryLightColor
        self.btnVerify.isHidden = true
        setupInitialView()
        // fetch data from the api
    }
    //MARK:-Actions
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnVerifyPressed(_ sender: Any){
        btnVerify.isHidden = true
        btnheightConstraints.constant = 0
        lblTimer.isHidden = false
        setUpTimer()
        runTimedCode()
        resendotp()
    }
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 400, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 9.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    //MARK:- Class Methods and Functions
    func resendotp()  {
        let obj = verifyDeiverViewModel()
        obj.verifyDriver(contact: strMobileNumber, country_code: strCountry_Code){ [self] (isSuccess, Data,status,message)  in
            let datamessage = Data["message"] as! String
            if datamessage == "success" || datamessage == "Success"  {
                 let responseData = Data["data"] as! [String:Any]
                let driverId = responseData["id"] as! Int
                UserDefaults.standard.setValue(driverId, forKey: "driverID")
               // self.navigationController?.pushViewController(vc, animated: true)
            }else{
                //total enter mobile number
                showToast(message: "please enter the Mobile Number")
            }
        }
    }
    func otrpverify(){
        let FCMTOken = UserDefaults.standard.string(forKey: "FCMToken")
        print(FCMTOken!)
        strOtp = "\(txtOne.text!)\(txtSecond.text!)\(txtThird.text!)\(txtFourth.text!)\(txtFive.text!)\(txtSix.text!)"
        print(strOtp)
        let obj = verifyOTPViewModel()
        obj.verifyOTP(contact: strMobileNumber, otp: strOtp, token: FCMTOken!, country_code: strCountry_Code) { (isSuccess, Data,message,statusCode) in
            SVProgressHUD.dismiss()
            if message == "Success" {
                //  if statusCode == 1 {
                print(Data)
                //   let otpVerifyDriverResponseData = Data["data"] as! [String:Any]
                UserDefaults.standard.setValue(true, forKey: "LoginSuccess")
                UserDefaults.standard.setValue(false, forKey: "LogoutSuccess")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
//                // self.navigationController?.pushViewController(vc, animated: true)
            }else{
                self.showToast(message: "OTP not matched")
            }
        }
    }
    
    func setupInitialView(){
        self.btnVerify.isHidden = true
        self.lblTimer.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        txtOne.layer.cornerRadius = 5.0
        txtOne.layer.masksToBounds = true
        //        txtOne.layer.borderWidth = 1
        //        txtOne.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtSecond.layer.cornerRadius = 5.0
        txtSecond.layer.masksToBounds = true
        //        txtSecond.layer.borderWidth = 1
        //  txtSecond.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtThird.layer.cornerRadius = 5.0
        txtThird.layer.masksToBounds = true
        //        txtThird.layer.borderWidth = 1
        //   txtThird.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtFourth.layer.cornerRadius = 5.0
        txtFourth.layer.masksToBounds = true
        //        txtFourth.layer.borderWidth = 1
        //  txtFourth.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtFive.layer.cornerRadius = 5.0
        txtFive.layer.masksToBounds = true
        //        txtFive.layer.borderWidth = 1
        // txtFive.layer.borderColor = kAppThemeborderColor.cgColor
        
        txtSix.layer.cornerRadius = 5.0
        txtSix.layer.masksToBounds = true
        //        txtSix.layer.borderWidth = 1
        //        txtSix.layer.borderColor = kAppThemeborderColor.cgColor
        
        //  lblTimer.textColor = kAppThemeborderColor
        
        btnVerify.layer.cornerRadius = 5.0
        btnVerify.layer.masksToBounds = true
        
        txtOne.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtSecond.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtThird.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtFourth.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtFive.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtSix.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        
        txtSix.addTarget(self, action: #selector(textfieldDidEndEditing), for: UIControl.Event.editingDidEnd)
        //self.setUpViewString()
        self.setUpTimer()
        
    }
    
    func setUpTimer(){
        lblTimer.isHidden = false
        btnVerify.isHidden = true
        btnheightConstraints.constant = 0
        remainnigSecond = 60
        lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(remainnigSecond)"
        objTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        self.setViewHideShowWithAnimarion(view: btnVerify, hidden: false)
    }
    @objc func  textfieldDidEndEditing(){
        if txtOne.text?.count == 1 {
            if txtSecond.text?.count == 1{
                if txtThird.text?.count == 1{
                    if txtFourth.text?.count == 1{
                        if txtFive.text?.count == 1{
                            if txtSix.text?.count == 1 {
                                SVProgressHUD.show()
                                otrpverify()
                            }
                        }
                    }
                }
            }
        }
    }
    @objc func runTimedCode(){
        if remainnigSecond == 0 {
            lblTimer.isHidden = true
            btnheightConstraints.constant = 45
            btnVerify.backgroundColor = kAppButtonBGDeactivateColor
            //  btnVerify.setTitle("Resend OTP", for: UIControl.State.normal)
            btnVerify.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: UIControl.State.normal)
            btnVerify.isHidden = false
            self.objTimer?.invalidate()
            txtOne.text = ""
            txtSecond.text = ""
            txtThird.text = ""
            txtFourth.text = ""
            txtFive.text = ""
            txtSix.text = ""
            txtOne.becomeFirstResponder()
            // verifyOtpApi()
        }else{
            remainnigSecond = remainnigSecond - 1
            let strRemain = String(format: "%02d", remainnigSecond)
            
            lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(strRemain)"
        }
    }
    //MARK:- UITextField Delegate Method
    
    @objc func textFieldEditingDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtOne:
                txtSecond.becomeFirstResponder()
            case txtSecond:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFourth.becomeFirstResponder()
            case txtFourth:
                txtFive.becomeFirstResponder()
            case txtFive:
                txtSix.becomeFirstResponder()
            case txtSix:
                txtSix.resignFirstResponder()
            default:
                break
            }
        }
        
        if  text?.count == 0 {
            switch textField{
            case txtOne:
                txtOne.becomeFirstResponder()
            case txtSecond:
                txtOne.becomeFirstResponder()
            case txtThird:
                txtSecond.becomeFirstResponder()
            case txtFourth:
                txtThird.becomeFirstResponder()
            case txtFive:
                txtFourth.becomeFirstResponder()
            case txtSix:
                txtFive.becomeFirstResponder()
            default:
                break
            }
        }else{
            //btnVerify.tintColor = .white
            btnVerify.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
            btnVerify.backgroundColor = kAppButtonBGDeactivateColor
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField.text!.count == 1{
            return false
        }
        
        return string.isNumber
        
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
