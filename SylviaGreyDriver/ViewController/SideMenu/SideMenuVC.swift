//
//  SideMenuVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {

    //MARK:Variables
    var arrayItems = ["SUMMARY","PERFORMANCE","REPORT","ACCOUNT","FAQs","NOTIFICATIONS","LOGOUT"]
    
    //MARK:Outlets
    @IBOutlet weak var tblSideMenuItems: UITableView!
    @IBOutlet weak var imgicon: UIImageView!
    
    @IBOutlet var viewMain: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.backgroundColor = kAppThemePrimaryLightColor
        viewMain.backgroundColor = kAppThemePrimaryLightColor
        //Register Xib
        let nib = UINib(nibName: "SideMenuItemsCell", bundle: nil)
        tblSideMenuItems.register(nib, forCellReuseIdentifier: "SideMenuItemsCell")
        tblSideMenuItems.backgroundColor = kAppThemePrimaryLightColor
        tblSideMenuItems.separatorStyle = UITableViewCell.SeparatorStyle.none
        setNeedsStatusBarAppearanceUpdate()
    }
    //MARK:Actions
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnClosePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension SideMenuVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuItemsCell") as! SideMenuItemsCell
        cell.lblItemTitle.text = arrayItems[indexPath.row]
        cell.contentView.backgroundColor = kAppThemePrimaryLightColor
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SummeryVC") as! SummeryVC
            self.presentInFullScreen(objVC, animated: false)
        }else if indexPath.row == 1{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "PerformanceVC") as! PerformanceVC
            objVC.modalPresentationStyle = .fullScreen
            self.present(objVC, animated: false, completion: nil)
        }else if indexPath.row == 2{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
            self.presentInFullScreen(objVC, animated: false)
        }else  if indexPath.row == 3{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
            self.presentInFullScreen(objVC, animated: false)
        }else if indexPath.row == 4{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "FAQsVC") as! FAQsVC
            self.presentInFullScreen(objVC, animated: false)
        }else if indexPath.row == 5{
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            self.presentInFullScreen(objVC, animated: false)
        }else if indexPath.row == 6{
            UserDefaults.standard.setValue(false, forKey: "LoginSuccess")
            UserDefaults.standard.setValue(true, forKey: "LogoutSuccess")
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.presentInFullScreen(objvc, animated: false)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
