//
//  NotificationVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

    //MARK:Outlets
    
    @IBOutlet weak var viewStatusBG: UIView!
    @IBOutlet weak var lblTitleTop: UILabel!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var tblNotification: UITableView!
    
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    var responseValue = [[String:Any]]()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.navigationBar.isHidden = true
        apicalling()
        //Register Xib
        let nib = UINib(nibName: "NotificationCell", bundle: nil)
        tblNotification.register(nib, forCellReuseIdentifier: "NotificationCell")
        tblNotification.tableFooterView = UIView() // Removes empty cell separators
        tblNotification.separatorStyle = UITableViewCell.SeparatorStyle.none
        tblNotification.layer.cornerRadius = 20.0
    }
    func apicalling()  {
        WebServiceCall.callMethodWithURL(route: .notification(driver_id:drive_id )) { (statusCode, responseData, error, response) in
            print("responseValue=\(responseData as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((statusCode))")
            if statusCode != 200 {
                if let dictResult = responseData{
                    if dictResult.isEmpty {
                        print(error)
                       // completion(false, responseValue!,responseValue!["status"] as! Int)
                    }
                }
            }else{
                if let dictResult = responseData{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                       // self.responseValue = arrResult
                        print(self.responseValue)
                        self.setUpDataInDescendingFormat(array: arrResult)
                       // self.tblNotification.reloadData()
                    }
                }
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setUpDataInDescendingFormat(array:[[String:Any]]){
        
        var convertedArray = [[String:Any]]()
        convertedArray = array.reversed()
        print("\(convertedArray)")
        responseValue = convertedArray
        tblNotification.reloadData()
    }
    @IBAction func btnBackPressed(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 10
        cell.viewNotification.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.layer.shadowRadius = 2
        cell.layer.shadowOpacity = 0.3
        cell.layer.masksToBounds = false
        cell.viewNotification.layer.shadowColor = UIColor.gray.cgColor
        cell.viewNotification.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.viewNotification.layer.shadowRadius = 2
        cell.viewNotification.layer.shadowOpacity = 0.3
        if responseValue.count != 0 {
            cell.lblTitle.text = responseValue[indexPath.row]["details"] as? String
            cell.lblDateTime.text = responseValue[indexPath.row]["created_at"] as? String
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
}
