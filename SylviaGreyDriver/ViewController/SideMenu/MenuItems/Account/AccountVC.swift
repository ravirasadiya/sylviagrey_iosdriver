//
//  AccountVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 26/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import iOSDropDown
import SVProgressHUD

class AccountVC: UIViewController,UITextFieldDelegate{

    //MARK:OUTLETS
    //On click of edit
    @IBOutlet weak var viewEditMobile: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtEditMobile: UITextField!
    @IBOutlet weak var txtEditCountryCode: DropDown!
    @IBOutlet weak var viewNameData: UIView!
    @IBOutlet weak var viewLocationData: UIView!
    @IBOutlet weak var viewNameDataSeprator: UIView!
    @IBOutlet weak var txtEditName: UITextField!
    @IBOutlet weak var txtEditEmail: UITextField!
    @IBOutlet weak var viewAccountEdit: UIView!
    
    @IBOutlet weak var viewEditCountryCode: UIView!
    @IBOutlet weak var viewMobileDataSeprator: UIView!
    @IBOutlet weak var viewMobileData: UIView!
    @IBOutlet weak var viewEmailDataSeprator: UIView!
    @IBOutlet weak var viewEmailData: UIView!
    @IBOutlet weak var viewLocationDataSeprator: UIView!
    //before edit click
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgMobile: UIImageView!
    @IBOutlet weak var txtMobileNumber: UITextField!
    //@IBOutlet weak var viewEditMobile: UIView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var viewEditEmail: UIView!
    @IBOutlet weak var imgName: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var viewEditName: UIView!
    @IBOutlet weak var lblProfileTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var viewTop: UIView!
    
    let driverInfo = UserDefaults.standard.value(forKey: "driverData") as? [String:Any]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewAccountEdit.isHidden = true
        setNeedsStatusBarAppearanceUpdate()
     //   DropDown()
        statusbar()
        addDoneButtonOnKeyboard()
        print(driverData)
       // txtEditCountryCode.text = "+91"
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        viewEditName.clipsToBounds = true
        viewEditMobile.clipsToBounds = true
        viewEditCountryCode.clipsToBounds = true
        viewEditEmail.clipsToBounds = true
        viewEditName.cornerRadius = 25
        viewEditEmail.cornerRadius = 25
        viewEditMobile.cornerRadius = 25
        viewEditCountryCode.cornerRadius = 25
        txtEditName.borderWidth = 0
        txtEditEmail.borderWidth = 0
        txtEditMobile.borderWidth = 0
        setupdata()
        txtEditMobile.delegate = self
        txtEditEmail.delegate = self
        txtEditName.delegate = self
        txtEditCountryCode.delegate = self
        txtName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtLocation.isUserInteractionEnabled = false
        txtMobileNumber.isUserInteractionEnabled = false
        txtEditCountryCode.isUserInteractionEnabled = false
        txtEditMobile.isUserInteractionEnabled = false
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK:Functions
    func apicalling()  {
        SVProgressHUD.show()
        let profile = driverInfo?["profile"] as? String ?? "null"
        let id = driverInfo?["id"] as? Int ?? 0
      //  let name = driverData["name"] as? String
        let status = driverInfo?["status"] as? Int ?? 0
        let vehicle_reg_no = driverInfo?["vehicle_reg_no"] as? String ?? "null"
        let vehicle_type = driverInfo?["vehicle_type"] as? Int ?? 0
        let licence_no = driverInfo?["licence_no"] as? String ?? "null"
        let bank_acc_no = driverInfo?["bank_acc_no"] as? String ?? "null"
        let bank_name = driverInfo?["bank_name"] as? String ?? "null"
        let acc_holder_name = driverInfo?["acc_holder_name"] as? String ?? "null"
        let country_code = driverInfo?["country_code"] as? String ?? "null"
        let postalcode = Int(driverInfo?["postalcode"] as? String ?? "null")
        let address = driverInfo?["address"] as? String ?? "null"
        let mobile = Int(txtEditMobile.text!)
        
        let param:[String : Any] = ["profile":profile,"id":id,"name":txtEditName.text!,"email":txtEditEmail.text!,"contact":mobile ?? 0,"status":status,"postcode":"\(postalcode ?? 0)","address":"\(address)","vehicle_reg_no":"\(vehicle_reg_no)","vehicletype":vehicle_type ,"licence_no":"\(licence_no)","bank_account_no":"\(bank_acc_no)","bank_name":"\(bank_name)","account_hold_name":"\(acc_holder_name)","country_code":"\(country_code)"]
        
        if txtEditName.text?.isEmpty == false || txtEditEmail.text?.isEmpty == false || txtEditMobile.text?.isEmpty == false || txtEditCountryCode.text?.isEmpty == false {
           
            WebServiceCall.callMethodWithURL(route: .accountEdit(param)) {(statusCode, responseData, error, response) in
                print("responseValue=\(responseData as Any)")
                print("error=\(String(describing: error))")
                print("responseCode=\((statusCode))")
                SVProgressHUD.dismiss()
                if statusCode != 200 {
                    if let dictResult = responseData{
                        if dictResult.isEmpty {
                            SVProgressHUD.dismiss()
                            print(error)
                            // completion(false, responseValue!,responseValue!["status"] as! Int)
                        }
                    }
                }else{
                        SVProgressHUD.dismiss()
                    let alertViewController = UIAlertController(title: "SylviaGreyDriver", message: "Data Updated Successfully.", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "ok", style: .cancel) { [self] (action) -> Void in
                      //  self.dismiss(animated: true, completion: nil)
                        // Remove Key-Value Pair
                        UserDefaults.standard.removeObject(forKey: "driverData")
                        //RestData
                        print(param)
                        UserDefaults.standard.setValue(param , forKey: "driverData")
                        let dData = UserDefaults.standard.value(forKey: "driverData") as! [String:Any]
                        print(dData)
                        self.viewAccountEdit.isHidden = true
                        txtName.text = txtEditName.text!
                        txtEmail.text = txtEditEmail.text!
                        txtMobileNumber.text = "\(txtEditCountryCode.text!)\(txtEditMobile.text!)"
                        imgProfile.isHidden = false
                        lblProfileTitle.isHidden = false
                        viewNameData.isHidden = false
                        viewLocationData.isHidden = false
                        viewEmailData.isHidden = false
                        viewMobileData.isHidden = false
                        btnEdit.isHidden = false
                    }
                    alertViewController.addAction(okAction)
                    self.present(alertViewController, animated: true, completion: nil)
                    return
                }
            }
        }else{
            if txtEditName.text?.isEmpty == true{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj:"SylviaGreyDriver", messageObj: "Please Enter the Name", viewcontrolelr: self)
                return
            }
            if txtEditMobile.text?.isEmpty == true{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj:"SylviaGreyDriver", messageObj: "Please Enter the Mobile No", viewcontrolelr: self)
                return
            }
            if txtEditEmail.text?.isEmpty == true{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj:"SylviaGreyDriver", messageObj: "Please Enter the Email", viewcontrolelr: self)
                return
            }
            if txtEditCountryCode.text?.isEmpty == true{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj:"SylviaGreyDriver", messageObj: "Please Enter the Country Code", viewcontrolelr: self)
                return
            }
        }
    }
     
    
    func setupdata(){
        lblProfileTitle.text = driverInfo?["name"] as? String
        txtName.text = driverInfo?["name"] as? String
        txtEmail.text = driverInfo?["email"] as? String
        let address = driverInfo?["address"] as? String
//        if address == "" {
//            txtLocation.text = driverInfo?["address"] as? String
//        }
        txtEditName.text = driverInfo?["name"] as? String
        txtEditEmail.text = driverInfo?["email"] as? String
        txtMobileNumber.text = "\(driverInfo?["country_code"] as? String ?? "")\(driverInfo?["contact"] as? Int ?? 0)"
        txtEditMobile.text = "\(driverInfo?["contact"] as? Int ?? 0)"
        txtEditCountryCode.text = driverInfo?["country_code"] as? String ?? ""
    }

    
    //MARK:Actions
    
    @IBAction func btnBackPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnEditPressed(_ sender: Any) {
        viewAccountEdit.isHidden = false
        imgProfile.isHidden = true
        lblProfileTitle.isHidden = true
        viewNameData.isHidden = true
        viewLocationData.isHidden = true
        viewEmailData.isHidden = true
        viewMobileData.isHidden = true
        btnEdit.isHidden = true
               
    }
    
    @IBAction func btnSavePressed(_ sender: Any) {
        apicalling()
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        viewAccountEdit.isHidden = true
        imgProfile.isHidden = false
        lblProfileTitle.isHidden = false
        viewNameData.isHidden = false
        viewLocationData.isHidden = false
        viewEmailData.isHidden = false
        viewMobileData.isHidden = false
        btnEdit.isHidden = false
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title:NSLocalizedString("DONE", comment: ""), style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        txtEditName.inputAccessoryView = doneToolbar
        txtEditEmail.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction(){
        txtEditName.resignFirstResponder()
        txtEditEmail.resignFirstResponder()
    }
}
extension UIView {
  func setCorner(radius: CGFloat) {
        layer.cornerRadius = radius
        layer.borderWidth = 0
        clipsToBounds = true
    }
    func circleCorner() {
           superview?.layoutIfNeeded()
           setCorner(radius: 20)//frame.height / 2)
       }
}
