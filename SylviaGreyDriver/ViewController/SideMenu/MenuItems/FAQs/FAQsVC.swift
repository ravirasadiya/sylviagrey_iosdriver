//
//  FAQsVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 26/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit

class FAQsVC: UIViewController {

    //MARK:Variables
    var isExpanded:Bool?
    var datasource = [ExpandingTableViewCellContent]()
    var responseValue = [[String:Any]]()
    //MARK:Outleets
    @IBOutlet weak var tblFAQs: UITableView!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var viewTop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        viewTop.backgroundColor = kAppThemePrimaryLightColor
//        //Register Xib
//        let nib = UINib(nibName: "FaqCell", bundle: nil)
//        tblFAQs.register(nib, forCellReuseIdentifier: "FaqCell")
     
        setupinitialview()
        apicalling()
    }
    func apicalling()  {
        WebServiceCall.callMethodWithURL(route: .faqsDriver) { (statusCode, responseData, error, response) in
            print("responseValue=\(responseData as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((statusCode))")
            if statusCode != 200 {
                if let dictResult = responseData{
                    if dictResult.isEmpty {
                        print(error)
                       // completion(false, responseValue!,responseValue!["status"] as! Int)
                    }
                }
            }else{
                if let dictResult = responseData{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                        self.responseValue = arrResult
                        print(self.responseValue)
                        self.setupdata()
                        self.tblFAQs.reloadData()
                    }
                }
            }
        }
    }
    func setupdata() {
        for i in 0 ..< responseValue.count {
            print(i)
            let addData = ExpandingTableViewCellContent(title: responseValue[i]["Que"] as? String ?? "", subtitle: responseValue[i]["Ans"] as? String ?? "")
            self.datasource.append(addData)
        }

        tblFAQs.reloadData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setupinitialview(){
        tblFAQs.dataSource = self
        tblFAQs.delegate = self
        tblFAQs.tableFooterView = UIView() // Removes empty cell separators
        tblFAQs.estimatedRowHeight = 65
        tblFAQs.rowHeight = UITableView.automaticDimension
        tblFAQs.registerCell(cellIDs: [faqCell], isDynamicHeight: true, estimatedHeight: 45)
    }
    //MARK:Actions
    @IBAction func btnBackPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
}
extension FAQsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell") as! FaqCell
        if datasource.count != 0{
            cell.set(content: datasource[indexPath.row])
        }
        if isExpanded == true{
            cell.imgDropDown.image = #imageLiteral(resourceName: "down-color")
        }else{
            cell.imgDropDown.image = #imageLiteral(resourceName: "leftColor")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            isExpanded = false
        let content = datasource[indexPath.row]
            content.expanded = !content.expanded
            if content.expanded{
                isExpanded = true
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
