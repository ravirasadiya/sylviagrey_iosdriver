//
//  ReportVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 26/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import YYCalendar
import SVProgressHUD
class ReportVC: UIViewController {
    
    //MARK:Outlets
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var tblReport: UITableView!
    @IBOutlet weak var lblEndDateTitle: UILabel!
    @IBOutlet weak var lblStartDateTitle: UILabel!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var viewtop: UIView!
    
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var lblNoData: UILabel!
    var responseValue = [[String:Any]]()
    var responseMessage = ""
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    let now = Date()
    let userCalendar = Calendar.current
    let requestedComponents: Set<Calendar.Component> = [
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second
    ]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        viewtop.backgroundColor = kAppThemePrimaryLightColor
        txtStartDate.borderWidth = 2
        txtEndDate.borderWidth = 2
        txtStartDate.borderColor = .darkText
        txtEndDate.borderColor = .darkText
        txtStartDate.textColor = .gray
        txtEndDate.textColor = .gray
        self.txtStartDate.delegate = self
        self.txtEndDate.delegate = self
        imgSearch.addTapGesture(tapNumber: 1, target: self, action: #selector(searchtap))
        tblReport.tableFooterView = UIView() // Removes empty cell separators
        setNeedsStatusBarAppearanceUpdate()
        tblReport.isHidden = true
        lblNoData.isHidden = false
        // get the user's calendar
        let formatter = DateFormatter()
        //2016-12-08 03:37:22 +0000
        formatter.dateFormat = "dd/MM/yyyy"
        let dateString = formatter.string(from:now)
       
        // get the components
        // choose which date and time components are needed
       
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: now)
        // now the components are available
        let year =  dateTimeComponents.year   // 2016
        let month = "\((dateTimeComponents.month)!)" // 10
        if month.count == 1 {
            txtStartDate.text = "01/0\(month)/\(year!)"
        }else{
            txtStartDate.text = "01/\(month)/\(year!)"
        }
        
        txtEndDate.text = dateString
        //Register Xib
        let nib = UINib(nibName: "TodayCell", bundle: nil)
        tblReport.register(nib, forCellReuseIdentifier: "TodayCell")
        tblReport.tableFooterView = UIView() // Removes empty cell separators
        tblReport.separatorStyle = UITableViewCell.SeparatorStyle.none
      
    }
    override func viewDidLayoutSubviews() {
       // tblHeightConstraint.constant = tblReport.contentSize.height
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //Mark:-function
    @objc func searchtap(){
        if txtStartDate.text?.isEmpty == false && txtEndDate.text?.isEmpty == false{
            SVProgressHUD.show()
            apicalling()
        }else{
            if txtStartDate.text?.isEmpty == true{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj: "SylviaGreyDriver", messageObj: "Please Enter Start Date", viewcontrolelr: self)
                return
            }
            if txtEndDate.text?.isEmpty == true{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj: "SylviaGreyDriver", messageObj: "Please Enter End Date", viewcontrolelr: self)
                return
            }
        }
    }
    func apicalling()  {
        SVProgressHUD.show()
        var startDate = txtStartDate.text!
        var  endDate = txtEndDate.text!
        // get the user's calendar
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = formatter.string(from:now)
        let userCalendar = Calendar.current
        if endDate.contains(find: "/") == true {
            endDate = dateString
        }
        if startDate.contains(find: "/") == true{
            startDate = txtStartDate.text!
            let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: now)
            // now the components are available
            let year =  dateTimeComponents.year   // 2016
          //  let month = "\((dateTimeComponents.month)!)" // 10
            let date = startDate.prefix(2)
            let getmonth = startDate.prefix(5)
            let month = getmonth.suffix(2)
            if month.count == 1 {
                startDate = "\(year!)-0\(month)-\(date)"
            }else{
                
                startDate = "\(year!)-\(month)-\(date)"
            }
        }
        WebServiceCall.callMethodWithURL(route: .report(from_date: startDate, driver_id: drive_id, to_date: endDate)) { (statusCode, responseData, error, response) in
            print("responseValue=\(responseData as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((statusCode))")
            self.responseMessage = responseData?["message"] as! String
            if statusCode != 200 {
                if let dictResult = responseData{
                    if dictResult.isEmpty {
                        print(error)
                        // completion(false, responseValue!,responseValue!["status"] as! Int)
                    }
                }
            }else{
                if let dictResult = responseData{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                      
                        self.responseValue = arrResult
                        print(self.responseValue)
                        SVProgressHUD.dismiss()
                        if self.responseValue.count != 0{
                            //lblhide //tblunhide
                            self.lblNoData.isHidden = true
                            self.tblReport.isHidden = false
                            self.tblReport.reloadData()
                        }else{
                            //lblshow // tblhide
                            self.lblNoData.isHidden = false
                            self.lblNoData.center = self.tblReport.center //Int(tblReport.contentSize.height/2)
                            self.tblReport.isHidden = true
                        }
                        
                    }
                }
            }
        }
    }
    
    //MARK:Actions
    
    @IBAction func btnBackPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
      
    }
    func statusbar(){
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = kAppThemePrimaryDarkColor
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = kAppThemePrimaryDarkColor
        }
    }
}
extension ReportVC:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        textField.resignFirstResponder()
        let formatter = DateFormatter()
        //2016-12-08 03:37:22 +0000
        formatter.dateFormat = "dd/MM/yyyy"
        let now = Date()
        let dateString = formatter.string(from:now)
        NSLog("%@", dateString)
       
        if textField == txtStartDate{
            let calendar = YYCalendar(limitedCalendarLangType: .ENG,
                                      date: dateString,
                                      minDate: "",
                                      maxDate: dateString,
                                      format: "dd/MM/yyyy") { [weak self] date in
                print(date)
             
                self?.txtStartDate.text = date
          
            }
            calendar.show()
        }
    
        if textField == txtEndDate{
           //2021-03-10
            if txtStartDate.text?.isEmpty == false{
                let calendar = YYCalendar(limitedCalendarLangType: .ENG,
                                          date: dateString,
                                          minDate: "\(txtStartDate.text!)",
                                          maxDate: "",
                                          format: "dd/MM/yyyy") { [weak self] date in
                    self?.txtEndDate.text = date
                }
                calendar.show()
            }else{
                SVProgressHUD.dismiss()
                ShowAlertDisplay(titleObj: "SylviaGreyDriver", messageObj: "Please First Enter Start Date", viewcontrolelr: self)
                return
            }
        }
    }
    
}
extension ReportVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseMessage == "Success"{
            return responseValue.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodayCell") as! TodayCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 20.0
        cell.contentView.cornerRadius = 20.0
        cell.viewCell.cornerRadius = 20.0
        cell.viewTop.cornerRadius = 20
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        if responseValue.count != 0 {
            cell.lblTitle.text = "\(responseValue[indexPath.row]["id"] as! Int)"
            cell.lblName.text = responseValue[indexPath.row]["customer_name"] as? String
            cell.lblLocation.text = responseValue[indexPath.row]["address"] as? String
            cell.lblDate.text = "\(responseValue[indexPath.row]["collection_date"] as! String)  \(responseValue[indexPath.row]["collection_time"] as! String)"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
  
}
