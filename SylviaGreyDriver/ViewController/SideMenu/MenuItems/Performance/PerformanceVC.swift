//
//  PerformanceVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 26/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
//import MBCircularProgressBar
import SVProgressHUD
class PerformanceVC: UIViewController {
    //MARK:Outlets
    @IBOutlet weak var viewPending: UIView!
    @IBOutlet weak var ViewDropUp: UIView!
    @IBOutlet weak var viewPickUp: UIView!
    @IBOutlet weak var viewStatusBG: UIView!
    @IBOutlet weak var viewTopTitle: UILabel!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var circularProgress: UIView!
    //  @IBOutlet weak var circularProgress: CirculerProgressView!
    @IBOutlet weak var viewlabel: UIView!
    @IBOutlet weak var view_Pending: UIView!
    @IBOutlet weak var viewDropup: UIView!
    @IBOutlet weak var viewPickup: UIView!
    @IBOutlet weak var lblPickUp: UILabel!
    
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblPending: UILabel!
    @IBOutlet weak var lblDropUp: UILabel!
    
    var circularProgressViewBar = PerformanceProgressBar()
    private var labelprogress = UILabel()
    var radius: CGFloat!
    var progress: CGFloat!
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    var responseValue = [String:Any]()
    var end = 0
    var start = 0
    //ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        ViewDropUp.backgroundColor = kAppThemePrimaryLightColor
        viewPickUp.backgroundColor =  kAppThemePrimaryLightColor
        view_Pending.backgroundColor = kAppThemePrimaryLightColor
        viewDropup.addShadow()
        viewPickup.addShadow()
        viewPending.addShadow()
        viewPickUp.addShadow()
        view_Pending.addShadow()
        ViewDropUp.addShadow()
        
        viewPickUp.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        ViewDropUp.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view_Pending.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        end = 55
        start = 100
        //Configure Progress Bar
        radius = (circularProgress.frame.height)/2.60
        progress = CGFloat(end) / CGFloat (start)
        let frameWidth: CGFloat  = 339
        let frameHeight:CGFloat  = 160
        
        circularProgressViewBar.addProgressBar(radius: radius, progress: progress)
        circularProgressViewBar.center = circularProgress.center
        circularProgressViewBar.frame = CGRect(x: circularProgress.bounds.width / 2 ,y: circularProgress.bounds.height / 2,width: frameWidth, height: frameHeight)
        //Adding view
        circularProgress.addSubview(circularProgressViewBar)
        
       // lblPercentage.center = CGPoint(x: circularProgressViewBar.bounds.midX, y: circularProgressViewBar.bounds.midY)
        
        apicalling()
       
    }

    func apicalling()  {
        SVProgressHUD.show()
        WebServiceCall.callMethodWithURL(route: .performance(driver_id: drive_id)) { (statusCode, responseData, error, response) in
            print("responseValue=\(responseData as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((statusCode))")
            if statusCode != 200 {
                if let dictResult = responseData{
                    if dictResult.isEmpty {
                        print(error)
                       // completion(false, responseValue!,responseValue!["status"] as! Int)
                    }
                }
            }else{
                if let dictResult = responseData{
                    SVProgressHUD.dismiss()
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [String:Any]{
                        self.responseValue = arrResult
                        print(self.responseValue)
                        self.lblPending.textColor = kAppThemePrimaryLightColor
                        self.lblPickUp.textColor = kAppThemePrimaryLightColor
                        self.lblDropUp.textColor = kAppThemePrimaryLightColor
                        self.lblPickUp.text = "\(String(describing: self.responseValue["pick"] as! Int))"
                        self.lblDropUp.text = "\(String(describing: self.responseValue["drop"] as! Int))"
                        self.lblPending.text = "\(String(describing: self.responseValue["pending"] as! Int))"
                    }
                }
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        apicalling()
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    func statusbar(){
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = kAppThemePrimaryDarkColor
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = kAppThemePrimaryDarkColor
        }
    }
}
extension UIView {
    func addShadow(){
        self.layer.cornerRadius = 7
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.7
    }
}
