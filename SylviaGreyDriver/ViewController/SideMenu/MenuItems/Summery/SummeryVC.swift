//
//  SummeryVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 30/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class SummeryVC: UIViewController {

    //MARK:Outlets
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var tblSummery: UITableView!
    @IBOutlet weak var viewTop: UIView!
    
    var responseValue = [[String:Any]]()
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        viewTop.backgroundColor = kAppThemePrimaryLightColor
        setNeedsStatusBarAppearanceUpdate()
        SVProgressHUD.show()
        apicalling()
        //Register Xib
        let nib = UINib(nibName: "SummeryCell", bundle: nil)
        tblSummery.register(nib, forCellReuseIdentifier: "SummeryCell")
        tblSummery.tableFooterView = UIView() // Removes empty cell separators
        tblSummery.separatorStyle = UITableViewCell.SeparatorStyle.none
        tblSummery.isHidden = true
        
        lblNoData.isHidden =  true
        //tblSummery.layer.cornerRadius = 20.0
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func apicalling()  {
        WebServiceCall.callMethodWithURL(route: .summary(driver_id: drive_id)) { (statusCode, responseData, error, response) in
            print("responseValue=\(responseData as Any)")
            print("error=\(String(describing: error))")
            print("responseCode=\((statusCode))")
            if statusCode != 200 {
                if let dictResult = responseData{
                    if dictResult.isEmpty {
                        print(error)
                       // completion(false, responseValue!,responseValue!["status"] as! Int)
                    }
                }
            }else{
                if let dictResult = responseData{
                    //  if dictResult["status"] as? String == "Success"{
                    if  let arrResult = dictResult["data"] as? [[String:Any]]{
                        self.responseValue = arrResult
                        print(self.responseValue)
                        SVProgressHUD.dismiss()
                        if self.responseValue.count != 0{
                            //lblhide //tblunhide
                            self.lblNoData.isHidden = true
                            self.tblSummery.isHidden = false
                            self.tblSummery.reloadData()
                        }else{
                            //lblshow // tblhide
                            self.lblNoData.isHidden = false
                            self.tblSummery.isHidden = true
                        }
                    }
                }
            }
        }
    }
    @IBAction func btnBackPressed(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
       // self.dismiss(animated: true, completion: nil)
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
extension SummeryVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseValue.count != 0 {
            return responseValue.count
        }else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummeryCell") as! SummeryCell
        
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 10
        cell.contentView.cornerRadius = 10
        cell.viewSummeryCell.cornerRadius = 10
        cell.viewSummeryCell.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner,.layerMaxXMaxYCorner]
        cell.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cell.layer.shadowRadius = 5
        cell.layer.shadowOpacity = 0.7
        cell.viewSummeryCell.layer.masksToBounds = false
        cell.viewSummeryCell.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cell.viewSummeryCell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.viewSummeryCell.layer.shadowRadius = 5
        cell.viewSummeryCell.layer.shadowOpacity = 0.7
        cell.viewPerson.cornerRadius = 10
        cell.viewPerson.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        if responseValue.count != 0 {
            cell.lblDate.text = responseValue[indexPath.row]["date"] as? String
            cell.lblProfile.text = responseValue[indexPath.row]["title"] as? String
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
