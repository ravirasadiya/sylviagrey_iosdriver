//
//  LoginVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 25/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import iOSDropDown
import SVProgressHUD
class LoginVC: UIViewController,UITextFieldDelegate{
    
    var contact = ""
    //MARK:Outlets
    @IBOutlet weak var viewStatusbar: UIView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var txtCountryCode: DropDown!
    @IBOutlet weak var txtMobileNumber: UITextField! //  login = "7600715726"
    @IBOutlet weak var viewStatusBarHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBackground: UIView!
    
    //ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.navigationBar.isHidden = true
        viewStatusBarHeight.constant = 0
        viewBackground.backgroundColor = kAppThemePrimaryLightColor
        txtMobileNumber.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        addDoneButtonOnKeyboard()
        txtMobileNumber.keyboardType = .phonePad
        DropDown()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
//        contact = txtMobileNumber.text!
//        UserDefaults.standard.setValue(txtMobileNumber.text!, forKey: "contact")
    }
    override func viewWillAppear(_ animated: Bool) {
//        let data = UserDefaults.standard.string(forKey: "contact")
//        if data?.isEmpty != true{
//            txtMobileNumber.text = data
//        }
    }
    //MARK:Functions
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 350, height: 35))
        toastLabel.center.x = self.view.center.x
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 5)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        btnContinue.tintColor = .white
        if (textField.text!.count == 0){
            btnContinue.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            btnContinue.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
            btnContinue.backgroundColor = kAppButtonBGDeactivateColor
            btnContinue.isUserInteractionEnabled = false
        }
        if textField == txtMobileNumber{
            let phone = txtMobileNumber.text
            if phone!.count > 6 {
                btnContinue.tintColor = .white
                btnContinue.setTitleColor(kAppButtonTextActiveColor, for: .normal)
                btnContinue.backgroundColor = kAppButtonBGActiveColor
                btnContinue.isUserInteractionEnabled = true
            }else{
                
                btnContinue.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                btnContinue.backgroundColor = kAppButtonBGDeactivateColor
                btnContinue.setTitleColor(kAppButtonTextDeactivateColor, for: .normal)
                btnContinue.isUserInteractionEnabled = false
            }
        }
    }
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title:NSLocalizedString("DONE", comment: ""), style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        txtMobileNumber.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction(){
        txtMobileNumber.resignFirstResponder()
    }
    func DropDown(){
        txtCountryCode.text = "+91"
        txtCountryCode.textAlignment = .right
        txtCountryCode.optionArray = ["+91","+41","+44","+1","+61","+33","+49","+971"]
        txtCountryCode.didSelect{(selectedText , index ,id) in
            self.txtCountryCode.text = selectedText
        }
    }
    
    @IBAction func btnContinuePressed(_ sender: Any) {
        SVProgressHUD.show()
        if txtMobileNumber.text!.isEmpty == true {
            SVProgressHUD.dismiss()
            showToast(message: "Please Enter Mobile No")
        }else if  txtMobileNumber.text!.count != 10 {
            SVProgressHUD.dismiss()
            showToast(message: "Please Enter the minimum 10 digits Mobile No")
        }else{
            contact = txtMobileNumber.text!
            let  country_code = txtCountryCode.text!
            let obj = verifyDeiverViewModel()
            obj.verifyDriver(contact: contact, country_code: country_code){ [self] (isSuccess, Data,status,message) in
                SVProgressHUD.dismiss()
               
                let datastatus =  Data["status"] as? Int ?? 00
                if message == "success"{
                    SVProgressHUD.dismiss()
                    let responseData = Data["data"] as! [String:Any]
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                    vc.strCountry_Code = country_code
                    vc.strMobileNumber = self.contact
                    let driverId = responseData["id"] as! Int
                    UserDefaults.standard.setValue(driverId, forKey: "driverID")
                    setupuserdefualts(data: responseData)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }else if message == "Driver not found"{
                    SVProgressHUD.dismiss()
                    showToast(message: "Driver Not Found")
                }
            }
        }
    }
    func setupuserdefualts(data:[String:Any])  {
        print(data)
        let driverData:[String : Any] = ["id":data["id"] ?? 0 , "vehicle_type": data["vehicle_type"] as? String ?? "",
                          "name": data["name"] as? String ?? "",
                          "email": data["email"] as? String ?? "",
                          "contact": data["contact"] as? Int ?? 0,
                          "country_code": data["country_code"] as? String ?? "",
                          "profile": data["profile"] as? String ?? "",
                          "status": data["status"] as? Int ?? 0,
                          "vehicle_reg_no": data["vehicle_reg_no"] as? String ?? "",
                          "licence_no": data["licence_no"] as? String ?? "",
                          "bank_acc_no":data["bank_acc_no"] as? String ?? "" ,
                          "bank_name": data["bank_name"] as? String ?? "",
                          "acc_holder_name": data["acc_holder_name"] as? String ?? "",
                          "postalcode": data["postalcode"] as? String ?? "",
                          "address": data["address"] as? String ?? ""]
        print(driverData)
        UserDefaults.standard.setValue(driverData, forKey: "driverData")
        print("UserDefualt driverData \(UserDefaults.standard.value(forKey: "driverData"))")
    
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if (textField.text?.count)! > 0 && textField != txtMobileNumber{
            return false
        }
        if textField == txtMobileNumber{
           // contact = txtMobileNumber.text!
            return range.location < 15
        }
        return string.isNumber
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}


