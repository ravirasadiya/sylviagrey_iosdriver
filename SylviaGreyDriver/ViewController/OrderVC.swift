//
//  OrderVC.swift
//  SylviaGreyDriver
//
//  Created by Shine on 30/12/20.
//  Copyright © 2020 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class OrderVC: UIViewController {
    
    
    let drive_id = UserDefaults.standard.integer(forKey: "driverID")
    var order_id:Int?
    var responsedata = [String:Any]()
    var responseItemData = [[String:Any]]()
    var pickup = ""
    var dropup = ""
    //MARK:Outlets
    @IBOutlet weak var tblviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblviewOrderItemsList: UITableView!
    @IBOutlet weak var viewMsgCustomer: UIView!
    @IBOutlet weak var viewCallCustomer: UIView!
    @IBOutlet weak var viewShowMap: UIView!
    @IBOutlet weak var viewMsgOffice: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPickUpTime: UILabel!
    @IBOutlet weak var lblDropoutDate: UILabel!
    @IBOutlet weak var lblDropOutTime: UILabel!
    @IBOutlet weak var btnPickup: UIButton!
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblTotalPaid: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    let obj = orderDetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusbar()
        SVProgressHUD.show()
        apicall()
        setNeedsStatusBarAppearanceUpdate()
        viewShowMap.addShadow()
        viewMsgOffice.addShadow()
        viewMsgCustomer.addShadow()
        viewCallCustomer.addShadow()
        viewShowMap.addTapGesture(tapNumber: 1, target: self, action: #selector(ShowMap))
        if dropup != "" {
            btnPickup.setTitle("Drop Up", for: UIControl.State.normal)
            btnPickup.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: UIControl.State.normal)
            btnPickup.backgroundColor = kAppButtonBGDeactivateColor
           // btnPickup.borderColor = #colorLiteral(red: 0, green: 0.5364905319, blue: 0.5364905319, alpha: 1)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setData()  {
        if responsedata.isEmpty != true {
            lblAddress.text = responsedata["address"] as! String
            lblTotalPaid.text = responsedata["total"] as? String
            lblPickUpDate.text = responsedata["collection_date"] as? String
            lblPickUpTime.text = responsedata["collection_time"] as? String
            lblDropoutDate.text = responsedata["delivery_date"] as? String
            lblDropOutTime.text = responsedata["delivery_time"] as? String
            lblName.text = responsedata["customer_name"] as? String
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        apicall()
    }
    func apicall() {
        obj.orderDetail(order_id: order_id ?? 0) { [self] (isSuccess, data,statusCode) in
            SVProgressHUD.dismiss()
            print(data)
            if statusCode == 200 {
                responsedata = (data["data"] as? [String:Any])!
                responseItemData = (responsedata["item"] as? [[String:Any]])!
                tblviewOrderItemsList.reloadData()
                setData()
            }
        }
    }
    override func viewDidLayoutSubviews() {
        //tableview height Setup
        tblviewHeightConstraint.constant = tblviewOrderItemsList.contentSize.height
        //ScrollView
        scrollView.contentSize = CGSize(width: self.viewMain.frame.width , height: self.viewMain.frame.height + 100)
    }
    //Mark: Function
    @objc func ShowMap(){
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderShowMapVC") as! OrderShowMapVC
        objVC.name = "\(responsedata["customer_name"] as? String)"
        objVC.location = "\(responsedata["address"] as! String)"
        objVC.pickupDateTime = "\(responsedata["collection_date"] as! String), \(responsedata["collection_time"] as! String)"
        objVC.DropOutDateTime = "\(responsedata["delivery_date"] as! String), \(responsedata["delivery_time"] as! String)"
        objVC.LongitudeData = "\(responsedata["longitude"])"
        objVC.latitudeData = "\(responsedata["latitude"])"
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: true, completion: nil)
        //          self.presentInFullScreen(objVC, animated: true)
    }
    //Mark: Action
    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPickupOnClick(_ sender: UIButton) {
        SVProgressHUD.show()
        if sender.titleLabel?.text == "Drop Up" {
            let  obj = DropOrderViewModel()
            if order_id != 0 {
                obj.dropOrder(driver_id: drive_id,order_id: order_id!) { [self] (isSuccess, statusCode, responseData, message, responseValue) in
                    SVProgressHUD.dismiss()
                    let status = responseValue["status"] as? Int
                    if status != 0 {
                            print("DropOrder response = \(responseData)")
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
                            vc.str_tabName = "New"
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
//                        responseDropData = responseData
//                        responseMessage = message
                     
                      //  tblviewDrop.reloadData()
                    }else{
//                        responseMessage = message
                        print("DropOrder message = \(message)")
//                        tblviewDrop.reloadData()
                    }
                }
            }
        }else{
            let obj = PickUpOrderViewModel()
            obj.pickupOrder(driver_id: drive_id, order_id: order_id ?? 0) { (isSuccess, statusCode, responseData, message,response) in
                SVProgressHUD.dismiss()
                print("PickUp order Response = \(response)")
                if statusCode == 200{
                    let status = response["status"] as? Int
                    if status != 0 {
                        print(isSuccess)
                        print("PickUporder Response = \(response)")
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
                        vc.str_tabName = "New"
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }else{
                        print("\(response["message"] as! String)")
                    }
                }else{
                    print("No")
                }
            }
        }
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}

extension OrderVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseItemData.count != 0 {
            return responseItemData.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if responseItemData.count != 0 {
            cell?.textLabel?.text = "1x \(responseItemData[0]["product_name"] as! String)"
            cell?.detailTextLabel?.text  = "\(responseItemData[0]["product_price"] as! String)" //total_paid
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
