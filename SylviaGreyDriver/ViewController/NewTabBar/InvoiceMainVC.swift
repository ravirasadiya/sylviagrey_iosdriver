//
//  InvoiceMainVC.swift
//  SylviaGreyDriver
//
//  Created by Apple on 16/03/21.
//  Copyright © 2021 Shine. All rights reserved.
//

import UIKit
import SVProgressHUD
class InvoiceMainVC: UIViewController {
    
    let driverId = UserDefaults.standard.integer(forKey: "driverID")
    var data = [[String:Any]]()
    //Mark:-Outlets
    
    @IBOutlet weak var viewnavigation: UIView!
    @IBOutlet weak var tblviewInvoice: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setNeedsStatusBarAppearanceUpdate()
        viewnavigation.backgroundColor = kAppThemePrimaryLightColor
        //Register Xib
        let nib = UINib(nibName: "InvoiceCell", bundle: nil)
        tblviewInvoice.register(nib, forCellReuseIdentifier: "InvoiceCell")
        tblviewInvoice.tableFooterView = UIView() // Removes empty cell separators
        tblviewInvoice.separatorStyle = UITableViewCell.SeparatorStyle.none
        //tblviewInvoice.layer.cornerRadius = 20.0
       // tblviewInvoice.estimatedRowHeight = UITableView.automaticDimension
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show()
        let obj =  CompleteOrderListForDriverViewModel()
        obj.invoiceData(driver_id: driverId) { (isSuccess, data) in
            SVProgressHUD.dismiss()
            if isSuccess{
                self.data = (data["data"] as? [[String:Any]])!
                print("Invoice Api Response = \(data)")
                self.tblviewInvoice.reloadData()
            }
        }
    }
    @objc func btnInvoiceDetails(sender : UIButton!){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvioceDetailsViewController") as! InvioceDetailsViewController
        vc.modalPresentationStyle = .fullScreen
        vc.Data = ([data[sender.tag]] as? [[String:Any]])!
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnSideMenuPressed(_ sender: Any) {
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: true, completion: nil)
        
    }
    func statusbar(){
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = kAppThemePrimaryDarkColor
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = kAppThemePrimaryDarkColor
       }
   }
}
extension InvoiceMainVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count != 0 {
            return data.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceCell") as! InvoiceCell
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 10
        cell.contentView.cornerRadius = 10
        cell.viewCell.cornerRadius = 10
        cell.viewTop.cornerRadius = 10
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 0.7
        cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.lblName.text = data[indexPath.row]["name"] as? String
        let address = data[indexPath.row]["address"] as! String
        let str1 = address.replacingOccurrences(of: ", ,", with: ",")
        cell.lblLocation.text = str1.replacingOccurrences(of: ", ,", with: ",")
        cell.lblDate.text = data[indexPath.row]["collection_date"] as? String
        cell.lblTitle.text = "\(data[indexPath.row]["order_id"] as! Int)"
        cell.btnPickUpHistory.tag = indexPath.row
        cell.btnPickUpHistory.addTarget(self, action: #selector(btnInvoiceDetails(sender:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // 200
        return 210
        //UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvioceDetailsViewController") as! InvioceDetailsViewController
        vc.modalPresentationStyle = .fullScreen
        vc.order_id = "\(data[indexPath.row]["order_id"] as! Int)"
        vc.Data = ([data[indexPath.row]] as? [[String:Any]])!
        self.present(vc, animated: false, completion: nil)
    }
}
