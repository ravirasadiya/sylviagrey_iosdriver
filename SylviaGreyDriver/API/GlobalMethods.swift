//
//  GlobalMethods.swift
import Foundation
import UIKit
import LGSideMenuController
import SafariServices
import Alamofire

class GlobalMethods: NSObject {
    
    
    //MARK: - User Default Functions

    class func setUserDefaultObject(object : AnyObject, key : String) {

         do {

             if #available(iOS 11.0, *) {

                 let data = try NSKeyedArchiver.archivedData(withRootObject: object, requiringSecureCoding: false)

                 UserDefaults.standard.set(data, forKey: key)

             } else {

                 // Fallback on earlier versions

                 let data: NSData = NSKeyedArchiver.archivedData(withRootObject: object) as NSData

                 UserDefaults.standard.set(data, forKey: key)

             }

             UserDefaults.standard.synchronize()

         } catch {

             print("Couldn't read file.")

         }

     }
    
    class func getUserDefaultObjectForKey(key : String) -> AnyObject? {

         if let data: AnyObject =  UserDefaults.standard.object(forKey: key) as AnyObject? {

             do {

                 if let retval = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data) as AnyObject? {

                     return retval

                 }

             } catch {

                 print("Couldn't read file.")

             }

         }

         return nil

     }
    //MARK: - Show error alert from API
    
//    class func showErrorFromAPI (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?, viewCtr : UIViewController?) {
//
//        if (error != nil)
//        {
//            if (error! as NSError).code != NSURLErrorCancelled {
//
//                if (error is AFError) {
//                    self.showAlert(alertTitle: NSLocalizedString(StringFile.strAlert[kAppDelegate.userCurrentLanguage], comment: ""), alertMessage: StringFile.strUnknown_error[kAppDelegate.userCurrentLanguage], viewCtr: viewCtr)
//                }
//                else {
//                    self.showAlert(alertTitle: NSLocalizedString(StringFile.strError[kAppDelegate.userCurrentLanguage], comment: ""), alertMessage: (error?.localizedDescription)!, viewCtr: viewCtr)
//                }
//            }
//        }
//        else
//        {
//            //response data is not in valid formate
//            self.showAlert(alertTitle: NSLocalizedString(StringFile.strAlert[kAppDelegate.userCurrentLanguage], comment: ""), alertMessage: StringFile.strUnknown_error[kAppDelegate.userCurrentLanguage], viewCtr: viewCtr)
//        }
//
//    }
    
    
    // MARK: - show alert view controller
    class func showAlert(alertTitle : String, alertMessage : String, viewCtr : UIViewController?, completion: (() -> Swift.Void)? = nil) {
        
        let alertViewController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString(StringFile.strOK[kAppDelegate.userCurrentLanguage], comment: ""), style: .cancel) { (action) -> Void in
            
            if (completion != nil) {
                completion!()
            }
        }
        
        alertViewController.addAction(okAction)
        
        viewCtr?.present(alertViewController, animated: true, completion: nil)
    }
    
    class func showAlert(alertTitle : String, alertMessage : String, okActionTitle : String, viewCtr : UIViewController?, completion: (() -> Swift.Void)? = nil) {
        
        let alertViewController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okActionTitle, style: .cancel) { (action) -> Void in
            
            if (completion != nil) {
                completion!()
            }
        }
        
        alertViewController.addAction(okAction)
        
        viewCtr?.present(alertViewController, animated: true, completion: nil)
    }
    
    // MARK: - show alert with 2 custom actions
    class func showAlert(alertTitle : String, alertMessage : String, okActionTitle : String,  cancelActionTitle : String, viewCtr : UIViewController?, successCompletion: (() -> Void)?, cancelCompletion: (() -> Void)?) {
        
        let alertViewController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: cancelActionTitle, style: .cancel) { (action) -> Void in
            
            if (cancelCompletion != nil) {
                cancelCompletion!()
            }
        }
        
        alertViewController.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: okActionTitle, style: .default) { (action) -> Void in
            
            if (successCompletion != nil) {
                successCompletion!()
            }
        }
        
        alertViewController.addAction(okAction)
        
        viewCtr?.present(alertViewController, animated: true, completion: nil)
    }
    
    // MARK: - show alert with 2 custon actions
    class func showAlert(alertTitle : String, alertMessage : String, destructiveActionTitle : String,  cancelActionTitle : String, viewCtr : UIViewController?, successCompletion: (() -> Void)?, cancelCompletion: (() -> Void)?) {
        
        let alertViewController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: cancelActionTitle, style: .cancel) { (action) -> Void in
            
            if (cancelCompletion != nil) {
                cancelCompletion!()
            }
        }
        
        alertViewController.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: destructiveActionTitle, style: .destructive) { (action) -> Void in
            
            if (successCompletion != nil) {
                successCompletion!()
            }
        }
        
        alertViewController.addAction(okAction)
        
        viewCtr?.present(alertViewController, animated: true, completion: nil)
    }
    
    //MARK: - custom bar button for navigation item
    class func cutomBarButtonItem(withImage image : UIImage, viewCtr : UIViewController, sel : Selector) -> UIBarButtonItem {
        
        let button = UIButton() as UIButton
        button.setImage(image, for: UIControl.State.normal)
        button.addTarget(viewCtr, action:sel, for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        let barButton = UIBarButtonItem(customView: button)
        
        return barButton
    }
    
    class func cutomTitleBarButtonItem(withTitle title : String, titleColor : UIColor, titleFont : UIFont, viewCtr : UIViewController, sel : Selector) -> UIBarButtonItem {
        
        let barButton = UIBarButtonItem(title: title, style: .plain, target: viewCtr, action: sel)
        barButton.setTitleTextAttributes([ NSAttributedString.Key.font : titleFont, NSAttributedString.Key.foregroundColor : titleColor], for: .normal)
        barButton.setTitleTextAttributes([ NSAttributedString.Key.font : titleFont, NSAttributedString.Key.foregroundColor : titleColor], for: .highlighted)
        
        return barButton
    }
    
    
    // MARK: - Push view controller
    class func pushToMenuViewCtr(withViewController viewCtr : UIViewController?, animated : Bool, fromSignUp : Bool) {
        
//        let obj_HomeViewCtr = viewCtr?.storyboard?.instantiateViewController(withIdentifier: "HomeViewCtr") as! HomeViewCtr
//
//        let nav_HomeViewCtr = UINavigationController(rootViewController: obj_HomeViewCtr)
//        nav_HomeViewCtr.isNavigationBarHidden = true
//
//        let obj_LeftMenuViewCtr = viewCtr?.storyboard?.instantiateViewController(withIdentifier: "LeftMenuViewCtr") as! LeftMenuViewCtr
//
//        let sideMenuViewCtr = LGSideMenuController()
//        sideMenuViewCtr.isLeftViewSwipeGestureEnabled = false
//        sideMenuViewCtr.rootViewController = nav_HomeViewCtr
//        sideMenuViewCtr.leftViewController = obj_LeftMenuViewCtr
//
//        sideMenuViewCtr.leftViewPresentationStyle = .slideAbove
//        sideMenuViewCtr.leftViewWidth = SystemSize.width - (SystemSize.width/4)
//
//        viewCtr?.navigationController?.pushViewController(sideMenuViewCtr, animated: animated)
    }
    
    class func pushToProductCtr(withViewController viewCtr : UIViewController?, animated : Bool, productId: String, attributeId: String = "") {
        
//        if let obj_ProductViewCtr = viewCtr?.storyboard?.instantiateViewController(withIdentifier: "ProductViewCtr") as? ProductViewCtr {
//            obj_ProductViewCtr.productId = productId
//            obj_ProductViewCtr.passedAttrbId = attributeId
//            viewCtr?.navigationController?.pushViewController(obj_ProductViewCtr, animated: animated)
//        }
        
    }
    
    // MARK: - Present view controller
    class func presentViewCtr(withViewController viewCtr : UIViewController?) {
        
        if let nav_ViewCtr = viewCtr?.storyboard?.instantiateViewController(withIdentifier: "") as? UINavigationController {
            
            if nav_ViewCtr.topViewController != nil {
                viewCtr?.present(nav_ViewCtr, animated: true, completion: nil)
            }
        }
    }
    
    class func presentLoaderViewCtr(withViewController viewCtr : UIViewController?) {
        
//        if let obj_LoaderViewCtr = viewCtr?.storyboard?.instantiateViewController(withIdentifier: "LoaderViewCtr") as? LoaderViewCtr {
//
//            obj_LoaderViewCtr.modalPresentationStyle = .overFullScreen
//            obj_LoaderViewCtr.modalTransitionStyle = .crossDissolve
//
//            viewCtr?.present(obj_LoaderViewCtr, animated: true, completion: nil)
//        }
    }
    
    class func dismissLoaderView(){
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name(kDismissLoaderView), object: nil)
        }
    }

    class func removeFile(atPath : String) {
        
        /*----- remove file at path -----*/
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: atPath) {
            try? fileManager.removeItem(atPath: atPath)
        }
    }
    
    class func saveImageOnDocDir(imageName: String, image: UIImage) {
        
        guard let documentsDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
    }
    
    class func getImageURLFromDiskWith(fileName: String) -> URL? {
        
        let documentDirectory = FileManager.SearchPathDirectory.cachesDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            return imageUrl
        }
        
        return nil
    }
    
    class func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let imageUrl = self.getImageURLFromDiskWith(fileName: fileName)

        if imageUrl != nil {
            let image = UIImage(contentsOfFile: imageUrl!.path)
            return image
        }
        
        return nil
    }
    
    class func clearCatchImages(){
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let documentPath = documentsURL.path
        
        do {
            let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
            for file in files {
                try fileManager.removeItem(atPath: "\(documentPath)/\(file)")
            }
        } catch {
            print("could not clear cache")
        }
    }
    
    // MARK:- Local Database
//    class func createDatabaseTbl(){
//
//        let strTblCreate = "CREATE TABLE IF NOT EXISTS cart_item(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, service_id VARCHAR, category_id VARCHAR, subcategory_id VARCHAR, customer_id VARCHAR, quantity VARCHAR, product_id VARCHAR, product_name VARCHAR, product_description VARCHAR, price VARCHAR)"
//        print(strTblCreate)
//        MySQL.CreateDBTable(Tblname: cart_item, createTblQuery: strTblCreate)
//
//    }
//    

    
    // MARK:- Validation

    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
                         "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
                         "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
                         "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
                         "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
                         "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
                         "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

    func validate(value: String) -> Bool {
            let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
            let result = phoneTest.evaluate(with: value)
            return result
        }
    
    // MARK:- Share Activity

    class func shareActivity(withText strShareText: String, withShareUrl strUrl: String, withViewController viewCtr : UIViewController) {
            
          
        let url = URL(string: strUrl)

        let objectsToShare = [strShareText, url as Any] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
        //Excluded Activities Code
        activityVC.excludedActivityTypes = [.print, .assignToContact, .saveToCameraRoll, .addToReadingList, .airDrop, .openInIBooks]
            
        viewCtr.present(activityVC, animated: true, completion: nil)
    }
    
    
    // MARK:- Globle API Call
    
//    class func getCartCount(){
//        WebServiceCall.callMethodWithURL(route: APIRouter.GetCountProduct(withCustId: kAppDelegate.userLoginData.id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
//
//            if (responseValue != nil) {
//
//                let responseModel = GetCartCountModel(dictionary: responseValue! as NSDictionary)
//
//                if responseModel.status == 200 {
//                    kAppDelegate.cartCount = responseModel.intData
//                    NotificationCenter.default.post(name: Notification.Name(kUpdateHomeCartCountNC), object: nil)
//                    NotificationCenter.default.post(name: NSNotification.Name(kUpdateProductViewCartCountNC), object: nil)
//                }
//            }
//        }
//    }
    
//    class func userLogoutApiCall(withToken token:String, completion: @escaping (_ success : Bool) -> ()){
//
//        let reqParam = [ParameterKeys.Customer_id : kAppDelegate.userLoginData.id, ParameterKeys.Token : token] as [String : Any]
//
//
//        WebServiceCall.callMethodWithURL(route: APIRouter.Logout(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
//
//             if (responseValue != nil) {
//
//                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
//
//                if responseModel.status == 200 {
//                    completion(true)
//                }else{
//                    completion(false)
//                }
//            }
//            completion(false)
//        }
//    }
    
//   class func logoutMoveToLoginScreen(withNavigation navView : UINavigationController?){
//        kAppDelegate.cartCount = 0
//        if let viewCtrs = navView?.viewControllers {
//            for controller in viewCtrs {
//                if controller.isKind(of: LoginViewCtr.self) {
//                    self.popToSpecificViewCtr(withViewCtr: controller, withNavigation: navView)
//                    return
//                }
//            }
//        }
//        self.popToSpecificViewCtr(withViewCtr: nil, withNavigation: navView)
//    }
    
//   class func popToSpecificViewCtr(withViewCtr controller : UIViewController?, withNavigation navView : UINavigationController?) {
//        kAppDelegate.userLoginData.delete()
//        if controller != nil {
//            navView!.popToViewController(controller!, animated: true)
//        }
//        else {
//            navView!.popToRootViewController(animated: true)
//        }
//    }
    
    class func SetUserDefualtLanguage(withLanguageType type: Int){
        UserDefaults.standard.removeObject(forKey: kLanguage)
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(type, forKey: kLanguage)
        UserDefaults.standard.synchronize()
        kAppDelegate.userCurrentLanguage = type
    }
    
//    class func updateLanguageForCurrentUser(){
//        
//        let strMyLanuageCode = kAppDelegate.userLoginData.language
//
//        switch strMyLanuageCode {
//        case "en":
//            kAppDelegate.userCurrentLanguage = 0
//        case "hi":
//            kAppDelegate.userCurrentLanguage = 1
//        case "gu":
//            kAppDelegate.userCurrentLanguage = 2
//        default:
//            kAppDelegate.userCurrentLanguage = 0
//        }
//        
//        self.SetUserDefualtLanguage(withLanguageType: kAppDelegate.userCurrentLanguage)
//    }
    
    class func getCurrentLanguageCode() -> String{
        var strMyLanguage = ""
        switch kAppDelegate.userCurrentLanguage {
        case 0:
            strMyLanguage = "en"
        case 1:
            strMyLanguage = "hi"
        case 2:
            strMyLanguage = "gu"
        default:
            strMyLanguage = "en"
        }
        return strMyLanguage
    }
    
    
}
extension UIColor {
    
    class func randomColor() -> UIColor {

        let hue = CGFloat(arc4random() % 100) / 100
        let saturation = CGFloat(arc4random() % 100) / 100
        let brightness = CGFloat(arc4random() % 100) / 100

        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
}
func generateRandomData() -> [[UIColor]] {
    let numberOfRows = 20
    let numberOfItemsPerRow = 15

    return (0..<numberOfRows).map { _ in
        return (0..<numberOfItemsPerRow).map { _ in UIColor.randomColor() }
    }
}


