//API Router
import UIKit
import Alamofire

enum APIRouter {
    
    case OtpVerify(Parameters)
    
    case verifyDriver(contact:String,country_code:String)
    
    case acceptorderRequest(order_id:Int)
    
    case declineorderRequest(order_id:String)
    
    case pendingRequest(driver_id:Int)
    
    case todayOrder(driver_id:Int)
    
    case tomorrowOrder(driver_id:Int)
    
    case pickupOrder(driver_id:Int,order_id:Int)
    
    case dropOrder(driver_id:Int,order_id:Int)
    
    case invoiceData(driver_id:Int)
    
    case orderDetail(order_id:Int)
    
    case notification(driver_id:Int)
    
    case summary(driver_id:Int)
    
    case report(from_date:String,driver_id:Int,to_date:String)
    
    case performance(driver_id:Int)
    
    case accountEdit(Parameters)
    
    case faqsDriver
}

extension APIRouter {
    
    var path: String{
        
        var url:String = ""
        
        switch self {
        
        case .accountEdit:
            url = "v1/updateProfileDriver"
        
        case .performance:
            url = "v1/performanceDriver"
        
        case .report:
            url = "v1/searchOrderDriver"
        
        case .summary:
            url = "v1/activityDriver"
        
        case .faqsDriver:
        url = "v1/faqDriver"
        
        case .notification:
            url = "v1/notificationDriver"
        
        case .orderDetail:
            url = "v1/orderDetailDriver"
        
        case .invoiceData:
            url = "v1/completeOrderListDriver"
            
        case .dropOrder:
            url = "v1/dropOrder"
        
        case .pickupOrder:
            url = "v1/pickupOrder"
        
        case . tomorrowOrder:
            url = "v1/tomorrowOrder"
            
        case .todayOrder:
            url = "v1/todayOrder"
            
        case .pendingRequest:
            url = "v1/pendingRequest"
            
        case .acceptorderRequest:
            url = "v1/requestAccept"
           
        case .declineorderRequest:
            url = "v1/requestDecline"
            
        case .verifyDriver:
            url = "v1/verifyDriver"
            
        case .OtpVerify:
            url = "v1/otpVerifyDriver"
            
        }
        
        return url
    }
    
    var baseURL: String {
        
        return StaticURL.APIBase + "api/"
    }
    
    var queryString: String {
        
        switch self {
      
            
        case .performance(let driver_id):
            return "?driver_id=\(driver_id)"
        
        case .report(let from_date,let driver_id,let to_date):
            return "?from_date=\(from_date)&driver_id=\(driver_id)&to_date=\(to_date)"
        
        case .summary(let driver_id):
            return "?driver_id=\(driver_id)"
        
        case .notification(let driver_id):
            return "?driver_id=\(driver_id)"
        
        case .orderDetail(let order_id):
            return "?order_id=\(order_id)"
        
        case . invoiceData(let driver_id):
            return "?driver_id=\(driver_id)"
            
        case .dropOrder(let driver_id,let order_id):
            return "?driver_id=\(driver_id)&order_id=\(order_id)"
        
        case .pickupOrder(let driver_id,let order_id):
            return "?driver_id=\(driver_id)&order_id=\(order_id)"
            
        case .tomorrowOrder(let driver_id):
            return "?driver_id=\(driver_id)"
            
        case .todayOrder(let driver_id):
            return "?driver_id=\(driver_id)"
            
        case .pendingRequest(let driver_id):
            return "?driver_id=\(driver_id)"
            
        case .verifyDriver(let contact, let country_code):
            return "?contact=\(contact)&country_code=\(country_code)"
        
        case .acceptorderRequest(let order_id):
            return "?order_id=\(order_id)"
            
        case .declineorderRequest(let order_id):
            return "?order_id=\(order_id)"
            
        default:
            return ""
        }
    }
    
    var method: HTTPMethod {
        
        switch self {
            
        case .verifyDriver,.acceptorderRequest,.declineorderRequest,.pendingRequest,.todayOrder,.tomorrowOrder,.pickupOrder,.dropOrder,.invoiceData,.orderDetail,.notification,.faqsDriver,.summary,.report,.performance:
            return .get
       
        default:
            return .post
        }
    }
    
    var parameters: Parameters? {

        switch self {
        
        case .accountEdit(let param):
            return param
//            return "?profile=\(profile)&id=5&name=\(name)&email=\(email)&contact=\(contact)&status=\(status)&postcode=\(postcode)&address=\(address)&vehicle_reg_no=\(vehicle_reg_no)&vehicletype=\(vehicletype)&licence_no=\(licence_no)&bank_account_no=\(bank_account_no)&bank_name=\(bank_name)&account_hold_name=\(account_hold_name)&country_code=\(country_code)"
        
        case .OtpVerify(let param):
            return param


        default:
            return nil
        }
    }
    
}
